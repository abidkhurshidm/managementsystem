﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using payrollado.Models;
using System.Data.Entity;
namespace payrollado.Controllers
{    
    public class EmployeeSalaryController : Controller
    {
        payrollEntities4 db = new payrollEntities4();
        // GET: EmployeeSalary
        public ActionResult Index(int? id)
        {
            if (Session["EmployeeId"] != null)
            {
                EmployeeSalaryM model = new EmployeeSalaryM();
                var res = db.EmployeeTypes.ToList();
                ViewBag.EmployeeTypeId = new SelectList(res, "EmployeeTypeId", "EmployeeType1");
                var res1 = db.LeaveTypes.ToList();
                ViewBag.LeaveTypeId = new SelectList(res1, "LeaveTypeId", "LeaveName");
                var res2 = db.Deductions.ToList();
                ViewBag.DeductionId = new SelectList(res2, "DeductionId", "DeductionName");
                ViewBag.EmployeeId = new SelectList("");
                ViewBag.AllowanceTypeId = new SelectList("");
                if(id.HasValue)
                {
                    var res3 = db.Employees.ToList();
                    var res4 = db.AllowanceTypes.ToList();
                    var empsal = db.EmployeeSalaries.Where(x => x.EmployeeSalaryId == id).FirstOrDefault();
                    model.BasicSelary = empsal.BasicSelary.GetValueOrDefault();
                    model.TotalAmount = empsal.TotalAmount.GetValueOrDefault();
                    model.hid = empsal.EmployeeSalaryId;
                    model.EmployeeId = empsal.EmployeeId.GetValueOrDefault();
                    ViewBag.EmployeeId = new SelectList(res3, "EmployeeId", "FirstName", empsal.EmployeeId);

                    var emptyp = db.Employees.Where(w => w.EmployeeId == empsal.EmployeeId).FirstOrDefault().EmployeeTypeId;
                    model.EmployeeTypeId = emptyp;

                    var alwtype = db.EmployeeAllowances.Where(w => w.EmployeeSalaryId == id).FirstOrDefault().AllowanceType.AllowanceTypeId;
                    ViewBag.AllowanceTypeId = new SelectList(res4, "AllowancetypeId", "AllowanceType1", alwtype);
                    model.employeeAllowances = db.EmployeeAllowances.Where(w => w.EmployeeSalaryId == empsal.EmployeeSalaryId).
                   Select(s => new EmployeeAllowenceM
                   {
                       AllowanceAmount = s.AllowanceAmount,
                       AllowanceTypeId = s.AllowanceTypeId,
                       AllowenceTypeName = s.AllowanceType.AllowanceType1,
                       EmployeeAllowanceId = s.EmployeeAllowanceId,
                       EmployeeSalaryId = s.EmployeeSalaryId
                   }).ToList();

                    model.employeeDeductions = db.EmployeeDeductions.Where(w => w.EmployeeSalaryId == empsal.EmployeeSalaryId).
                    Select(s => new EmployeedeductionM
                   {
                  EmployeeDedeuctionId = s.EmployeeDedeuctionId,
                  DeductionAmount = s.DeductionAmount,
                  EmployeeSalaryId = s.EmployeeSalaryId,
                  DeductionId = s.DeductionId,
                  deductionTypeName = s.Deduction.DeductionName
                   }).ToList();

                    model.EmployeeLeaves = db.EmployeeLeaves.Where(w => w.EmployeeSalaryId == empsal.EmployeeSalaryId).
                       Select(s => new EmployeeleaveM
                       {
                           EmployeeLeaveTypeId = s.EmployeeLeaveTypeId,
                           LeaveDays = s.LeaveDays,
                           LeaveTypeId = s.LeaveTypeId,
                           EmployeeSalaryId = s.EmployeeSalaryId,
                           leaveTypeName = s.LeaveType.LeaveName
                       }).ToList();
                    return View(model);
                }
                else
                {
                    model.employeeDeductions = null;
                    model.employeeAllowances = null;
                    model.EmployeeLeaves = null;
                    return View(model);
                }
            }

                return RedirectToAction("Index", "Login"); 
        }
        [HttpPost]
        public ActionResult Index(EmployeeSalaryM modal)
        {
            var msg = "";
            var res3 = db.Employees.ToList();
            ViewBag.EmployeeId = new SelectList(res3, "EmployeeId", "FirstName");
            var res = db.EmployeeTypes.ToList();
            ViewBag.EmployeeTypeId = new SelectList(res, "EmployeeTypeId", "EmployeeType1");
            var res1 = db.LeaveTypes.ToList();
            ViewBag.LeaveTypeId = new SelectList(res1, "LeaveTypeId", "LeaveName");
            var res2 = db.Deductions.ToList();
            ViewBag.DeductionId = new SelectList(res2, "DeductionId", "DeductionName");
            var res5 = db.AllowanceTypes.ToList();
            ViewBag.AllowanceTypeId = new SelectList(res5, "AllowanceTypeID", "AllowanceType1");

            var id = modal.hid;
            if (id == 0)
            {
                EmployeeSalary obj = new EmployeeSalary();
                obj.BasicSelary = modal.BasicSelary;
                obj.TotalAmount = modal.TotalAmount;
                obj.EmployeeId = modal.EmployeeId;
                obj.CreatedBy = modal.EmployeeId;
                obj.CreatedAt = DateTime.Now;
                db.EmployeeSalaries.Add(obj);
                db.SaveChanges();
                if (modal.employeeAllowances != null)
                {
                    foreach (var item in modal.employeeAllowances)
                    {
                        EmployeeAllowance obj2 = new EmployeeAllowance();
                        obj2.AllowanceAmount = item.AllowanceAmount;
                        obj2.AllowanceTypeId = item.AllowanceTypeId.GetValueOrDefault();
                        obj2.EmployeeSalaryId = obj.EmployeeSalaryId;
                        db.EmployeeAllowances.Add(obj2);
                    }
                }
                if (modal.employeeDeductions != null)
                {
                    foreach (var item2 in modal.employeeDeductions)
                    {
                        EmployeeDeduction obj3 = new EmployeeDeduction();
                        obj3.DeductionId = item2.DeductionId.GetValueOrDefault();
                        obj3.DeductionAmount = item2.DeductionAmount;
                        obj3.EmployeeSalaryId = obj.EmployeeSalaryId;
                        db.EmployeeDeductions.Add(obj3);
                    }
                }
                if (modal.EmployeeLeaves != null)
                {
                    foreach (var item3 in modal.EmployeeLeaves)
                    {
                        EmployeeLeave obj4 = new EmployeeLeave();
                        obj4.LeaveTypeId = item3.LeaveTypeId.GetValueOrDefault();
                        obj4.LeaveDays = item3.LeaveDays;
                        obj4.EmployeeSalaryId = obj.EmployeeSalaryId;
                        db.EmployeeLeaves.Add(obj4);
                    }
                }
                db.SaveChanges();
                 msg = "Save";
                return Json(msg, JsonRequestBehavior.AllowGet);
            }

            //var empchk = db.EmployeeAllowances.Where(w => w.EmployeeAllowanceId == modal ;



            var empsalu = db.EmployeeSalaries.Where(w => w.EmployeeSalaryId == id).FirstOrDefault();          
            empsalu.BasicSelary = modal.BasicSelary;
            empsalu.TotalAmount = modal.TotalAmount;
            empsalu.EmployeeId = modal.EmployeeId;
            empsalu.CreatedBy = modal.EmployeeId;
            empsalu.CreatedAt = DateTime.Now;
            db.SaveChanges();


            if(modal.deletealws != null)
            {
                foreach(var itemdel in modal.deletealws)
                {
                    var del = db.EmployeeAllowances.Where(w => w.EmployeeAllowanceId == itemdel.EmployeeAllowanceId).FirstOrDefault();
                    db.EmployeeAllowances.Remove(del);
                }
                db.SaveChanges();
            }

            if (modal.deletelevs != null)
            {
                foreach (var itemdel in modal.deletelevs)
                {
                    var del = db.EmployeeLeaves.Where(w => w.EmployeeLeaveTypeId == itemdel.EmployeeLeaveTypeId).FirstOrDefault();
                    db.EmployeeLeaves.Remove(del);
                }
                db.SaveChanges();
            }
            if (modal.deleteducs != null)
            {
                foreach (var itemdel in modal.deleteducs)
                {
                    var del = db.EmployeeDeductions.Where(w => w.EmployeeDedeuctionId == itemdel.EmployeeDedeuctionId).FirstOrDefault();
                    db.EmployeeDeductions.Remove(del);
                }
                db.SaveChanges();
            }


            if (modal.employeeAllowances != null)
            {

                foreach (var item in modal.employeeAllowances)
                {
                    EmployeeAllowance obj2 = new EmployeeAllowance();
                    var del = db.EmployeeAllowances.Where(w => w.EmployeeAllowanceId == item.EmployeeAllowanceId).FirstOrDefault();
                    if (del != null)
                    {
                        db.EmployeeAllowances.Remove(del);
                    }
                    obj2.AllowanceAmount = item.AllowanceAmount;
                    obj2.AllowanceTypeId = item.AllowanceTypeId.GetValueOrDefault();
                    obj2.EmployeeSalaryId = id;
                    db.EmployeeAllowances.Add(obj2);
                    db.SaveChanges();
                }
            }
            //var emplev = db.EmployeeLeaves.Where(w => w.EmployeeLeaveTypeId == modal.).FirstOrDefault();
            if (modal.EmployeeLeaves != null)
            {
                foreach (var item3 in modal.EmployeeLeaves)
                {
                    EmployeeLeave obj4 = new EmployeeLeave();
                    var del = db.EmployeeLeaves.Where(w => w.EmployeeLeaveTypeId == item3.EmployeeLeaveTypeId).FirstOrDefault();
                    if (del != null)
                    {
                        db.EmployeeLeaves.Remove(del);
                    }
                    obj4.LeaveTypeId = item3.LeaveTypeId.GetValueOrDefault();
                    obj4.LeaveDays = item3.LeaveDays;
                    obj4.EmployeeSalaryId = id;
                    db.EmployeeLeaves.Add(obj4);
                }
            }
            //var empduc = db.EmployeeDeductions.Where(w => w.EmployeeDedeuctionId == modal.Employeedeductionhid).FirstOrDefault();
            if (modal.employeeDeductions != null)
            {
                foreach (var item2 in modal.employeeDeductions)
                {
                    EmployeeDeduction obj3 = new EmployeeDeduction();
                    var del = db.EmployeeDeductions.Where(w => w.EmployeeDedeuctionId == item2.EmployeeDedeuctionId).FirstOrDefault();
                    if (del != null)
                    {
                        db.EmployeeDeductions.Remove(del);
                    }
                    obj3.DeductionId = item2.DeductionId.GetValueOrDefault();
                    obj3.DeductionAmount = item2.DeductionAmount;
                    obj3.EmployeeSalaryId = id;
                    db.EmployeeDeductions.Add(obj3);
                }
            }
            db.SaveChanges();
             msg = "Update";
            return Json(msg, JsonRequestBehavior.AllowGet);
           
        }
        //var alwid = db.EmployeeAllowances.Where(x => x. == modal.AllowanceTypeId).SingleOrDefault();
        //alwid.AlloawnceAmount = modal.AlloawnceAmount;
        //alwid.EmployeeId = modal.EmployeeId;
        //alwid.EmployeeSalaryId = obj3.EmployeeSalaryId;

        //var deduc = db.Deductions.Where(x => x.DeductionId == modal.DeductionId).SingleOrDefault();
        //deduc.DeductionAmount = modal.DeductionAmount;
        //deduc.EmployeeId = modal.EmployeeId;
        //deduc.EmployeeSalaryId = obj3.EmployeeSalaryId;

        //var levid = db.LeaveTypes.Where(x => x.LeaveTypeId == modal.LeaveTypeId).SingleOrDefault();
        //levid.LeaveDays = modal.LeaveDays;
        //levid.EmployeeId = modal.EmployeeId;
        //levid.EmployeeSalaryId = obj3.EmployeeSalaryId;   


        public ActionResult EmployeeId(int EmployeeTypeId)
        {

            return Json(db.AllowanceTypes.Where(s => s.EmployeeTypeId == EmployeeTypeId).Select(s => new
            {
                id = s.AllowanceTypeId,
                Name = s.AllowanceType1
            }).ToList(), JsonRequestBehavior.AllowGet);

        }

        public ActionResult EmployeeTypeId(int EmployeeTypeId)
        {
            return Json(db.Employees.Where(x => x.EmployeeTypeId == EmployeeTypeId).Select(x => new
            {
                id = x.EmployeeId,
                name = x.FirstName,
                lstName = x.LastName
            }).ToList(), JsonRequestBehavior.AllowGet);
        }

        //public ActionResult EmployeeidGet(int EmployeeTypeId)
        //{
        //    var res 

        //    return Json(db.Employees.Where(x => x.EmployeeTypeId == EmployeeTypeId).Select(x => new
        //    {
        //        id = x.EmployeeId,
        //        name = x.FirstName,
        //        lstName = x.LastName
        //    }).FirstOrDefault(), JsonRequestBehavior.AllowGet);
        //}
        public ActionResult list()
        {
           
            List<EmployeeSalary> employeeSalaries = db.EmployeeSalaries.ToList();
            List<Employee> employees  = db.Employees.ToList();

            var multiples = (from empsal in employeeSalaries
                             join emp in employees
                             on empsal.EmployeeId equals emp.EmployeeId
                             select new Sallist
                             {
                                 FirstName = emp.FirstName,
                                 LastName = emp.LastName,
                                 EmployeeId = emp.EmployeeId,
                                 EmployeeSalaryId = empsal.EmployeeSalaryId,
                                 BasicSelary = empsal.BasicSelary.GetValueOrDefault(),
                                 TotalAmount = empsal.TotalAmount.GetValueOrDefault()
                             }).ToList();
            return View(multiples);
        }

        public ActionResult update(int? id, int? EmpSal)
        {
            EmployeeSalaryM model = new EmployeeSalaryM();
            var res = db.EmployeeTypes.ToList();
            ViewBag.EmployeeTypeId = new SelectList(res, "EmployeeTypeId", "EmployeeType1");
            var res3 = db.Employees.ToList();
            ViewBag.EmployeeId = new SelectList(res3, "EmployeeId", "FirstName");
            var res4 = db.AllowanceTypes.ToList();
            ViewBag.AllowanceTypeId = new SelectList(res4, "AllowanceTypeID", "AllowanceType1");
            var res1 = db.LeaveTypes.ToList();
            ViewBag.LeaveTypeId = new SelectList(res1, "LeaveTypeId", "LeaveName");
            var res2 = db.Deductions.ToList();
            ViewBag.DeductionId = new SelectList(res2, "DeductionId", "DeductionName");

            var empsal = db.EmployeeSalaries.Where(x => x.EmployeeSalaryId == EmpSal).FirstOrDefault();
            model.BasicSelary = empsal.BasicSelary.GetValueOrDefault();
            model.TotalAmount = empsal.TotalAmount.GetValueOrDefault();
            model.hid = empsal.EmployeeSalaryId;
            model.EmployeeId = empsal.EmployeeId.GetValueOrDefault();

            var emptyp = db.EmployeeAllowances.Where(w => w.EmployeeSalaryId == empsal.EmployeeSalaryId).FirstOrDefault().AllowanceType.EmployeeTypeId;
            model.EmployeeTypeId = emptyp;


            model.employeeAllowances = db.EmployeeAllowances.Where(w => w.EmployeeSalaryId == empsal.EmployeeSalaryId).
                    Select(s => new EmployeeAllowenceM
                    {
                        AllowanceAmount = s.AllowanceAmount,
                        AllowanceTypeId = s.AllowanceTypeId,
                        AllowenceTypeName = s.AllowanceType.AllowanceType1,
                        EmployeeAllowanceId = s.EmployeeAllowanceId,
                        EmployeeSalaryId = s.EmployeeSalaryId
                    }).ToList();

          

                model.employeeDeductions = db.EmployeeDeductions.Where(w => w.EmployeeSalaryId == empsal.EmployeeSalaryId).
                   Select(s => new EmployeedeductionM
                   {
                       EmployeeDedeuctionId = s.EmployeeDedeuctionId,
                       DeductionAmount = s.DeductionAmount,
                       EmployeeSalaryId = s.EmployeeSalaryId,
                       DeductionId = s.DeductionId,
                       deductionTypeName = s.Deduction.DeductionName
                   }).ToList();
            model.EmployeeLeaves = db.EmployeeLeaves.Where(w => w.EmployeeSalaryId == empsal.EmployeeSalaryId).
               Select(s => new EmployeeleaveM
               {
            EmployeeLeaveTypeId = s.EmployeeLeaveTypeId, 
            LeaveDays = s.LeaveDays,
            LeaveTypeId =  s.LeaveTypeId,
            EmployeeSalaryId = s.EmployeeSalaryId, 
            leaveTypeName = s.LeaveType.LeaveName 
            }).ToList();
            return View(model);
            //    var allowance = db.EmployeeAllowances.Include(i => i.AllowanceType).Where(x => x.EmployeeSalaryId == empsal.EmployeeSalaryId).ToList();
            //    var deduction = db.EmployeeDeductions.Include(i => i.Deduction).Where(x => x.EmployeeSalaryId == empsal.EmployeeSalaryId).ToList();
            //    var leave = db.EmployeeLeaves.Include(i => i.LeaveType).Where(x => x.EmployeeSalaryId == empsal.EmployeeSalaryId).ToList();
            //    model.TypeNameAllowance = new List<string>();
            //    model.TypeNameDeduction = new List<string>();
            //    model.TypeNameLeaves = new List<string>();

            //    foreach (var item in allowance)
            //    {
            //        string data = db.AllowanceTypes.Where(w => w.AllowanceTypeId == item.AllowanceTypeId).FirstOrDefault().AllowanceType1;
            //        model.TypeNameAllowance.Add(data);
            //    }
            //    foreach (var item in deduction)
            //    {                    
            //        string data = db.Deductions.Where(w => w.DeductionId == item.DeductionId).FirstOrDefault().DeductionName;
            //        model.TypeNameDeduction.Add(data);
            //    }
            //    foreach (var item in leave)
            //    {    
            //        string data  =   db.LeaveTypes.Where(w => w.LeaveTypeId == item.LeaveTypeId).FirstOrDefault().LeaveName;
            //        model.TypeNameLeaves.Add(data);
            //    }

            //}

            //var empid = db.Employees.Where(x => x.EmployeeId == id).FirstOrDefault();
            //if(empid != null)
            //{
            //    model.EmployeeId = empid.EmployeeId;
            //    model.EmployeeTypeId = empid.EmployeeTypeId.GetValueOrDefault();

            //}

            //if (query.EmployeeAllowances != null)
            //    {
            //        model.employeeAllowances = query.EmployeeAllowances.ToList();
            //    var allowance = db.EmployeeSalaries.Where(x => x.EmployeeSalaryId == EmpSal).ToList();
            //}

            //if (query.EmployeeDeductions != null)
            //{
            //    model.employeeDeductions = query.EmployeeDeductions.ToList();
            //    var allowance = db.EmployeeSalaries.Where(x => x.EmployeeSalaryId == EmpSal).ToList();
            //}

            //if (query.EmployeeLeaves != null)
            //{
            //       model.EmployeeLeaves = query.EmployeeLeaves.ToList();
            //    var allowance = db.EmployeeSalaries.Where(x => x.EmployeeSalaryId == EmpSal).ToList();
            //}

            //var empid = db.Employees.Where(x=> x.EmployeeId == id).Select(s=> new )


            //using (var db = new payrollEntities4())
            // {
            //     var res = db.EmployeeTypes.ToList();
            //     ViewBag.EmployeeTypeId = new SelectList(res, "EmployeeTypeId", "EmployeeType1");
            //     var res3 = db.Employees.ToList();
            //     ViewBag.EmployeeId = new SelectList(res3, "EmployeeId", "FirstName");
            //     var res1 = db.LeaveTypes.ToList();
            //     ViewBag.LeaveTypeId = new SelectList(res1, "LeaveTypeId", "LeaveName");
            //     var res2 = db.Deductions.ToList();
            //     ViewBag.DeductionId = new SelectList(res2, "DeductionId", "DeductionName");
            //     var res4 = db.AllowanceTypes.ToList();
            //     ViewBag.AllowanceTypeId = new SelectList(res4, "AllowanceTypeID", "AllowanceType1");




            //     //var emtypid = db.EmployeeTypes.Where(x => x.EmployeeTypeId == model.EmployeeTypeId);

            //     var empid = db.EmployeeSalaries.Where(x => x.EmployeeId == id).FirstOrDefault();
            //     var empsal = db.EmployeeSalaries.Where(x => x.EmployeeSalaryId == empid.EmployeeSalaryId).FirstOrDefault();
            //     if (empsal != null)
            //     {
            //         model.BasicSelary = empsal.BasicSelary.GetValueOrDefault();
            //         model.TotalAmount = empsal.TotalAmount.GetValueOrDefault();
            //         model.CreatedBy = empsal.CreatedBy.GetValueOrDefault();
            //         model.CreatedAt = empsal.CreatedAt.GetValueOrDefault();
            //         model.EmployeeSalaryId = empsal.EmployeeSalaryId;
            //     }
            //     var altype = db.EmployeeAllowances.Where(x => x.EmployeeSalaryId == EmployeeSal).FirstOrDefault();
            //  //   var altype = db.EmployeeAllowances.Where(x => x.EmployeeSalaryId == empid2.EmployeeSalaryId).SingleOrDefault();
            //     if (altype != null)
            //    {
            //         model.AllowanceAmount = altype.AllowanceAmount.GetValueOrDefault();
            //         model.AllowanceType1 = altype.AllowanceType.AllowanceType1;                  
            //         model.AllowanceTypeId = altype.AllowanceTypeId.GetValueOrDefault();
            //    }
            //     //var empid3 = db.EmployeeSalaries.Where(x => x.EmployeeId == id).FirstOrDefault();
            //     var deduc = db.EmployeeDeductions.Where(x => x.EmployeeSalaryId == EmployeeSal).FirstOrDefault();
            //     if (deduc != null)
            //     {
            //         var dedcid = db.Deductions.Where(x => x.DeductionId == deduc.DeductionId).SingleOrDefault();
            //         model.DeductionAmount = deduc.DeductionAmount;
            //         model.DeductionName = dedcid.DeductionName;
            //         model.DeductionId = deduc.DeductionId.GetValueOrDefault();
            //     }

            //     var lvetyp = db.EmployeeLeaves.Where(x => x.EmployeeSalaryId == EmployeeSal).FirstOrDefault();
            //     if (lvetyp != null)
            //     {
            //         var lvecid = db.LeaveTypes.Where(x => x.LeaveTypeId == lvetyp.LeaveTypeID).FirstOrDefault();
            //         model.LeaveName = lvecid.LeaveName;
            //         model.LeaveDays = lvetyp.LeaveDays.GetValueOrDefault();
            //         model.LeaveTypeId = lvetyp.LeaveTypeID.GetValueOrDefault();
            //     }

            //     var empid5 = db.Employees.Where(x => x.EmployeeId == id).FirstOrDefault();
            //     if (empid5 != null)
            //     {
            //         model.EmployeeId = empid5.EmployeeId;
            //         model.EmployeeTypeId = empid5.EmployeeTypeId.GetValueOrDefault();
            //     }    

        }

        [HttpPost]
        public ActionResult update(EmployeeSalaryM model,int id)
        {

            var res = db.EmployeeTypes.ToList();
            ViewBag.EmployeeTypeId = new SelectList(res, "EmployeeTypeId", "EmployeeType1");
            var res2 = db.Deductions.ToList();
            ViewBag.DeductionId = new SelectList(res2, "DeductionId", "DeductionName");
            var res3 = db.Employees.ToList();
            ViewBag.EmployeeId = new SelectList(res3, "EmployeeId", "FirstName");
            var res4 = db.AllowanceTypes.ToList();
            ViewBag.AllowanceTypeId = new SelectList(res4, "AllowanceTypeID", "AllowanceType1");
            var res1 = db.LeaveTypes.ToList();
            ViewBag.LeaveTypeId = new SelectList(res1, "LeaveTypeId", "LeaveName");

            var alwid = db.EmployeeAllowances.Where(x => x.EmployeeSalaryId == id).SingleOrDefault();
            if (alwid != null)
            {
                var altyp = db.EmployeeAllowances.Where(x => x.EmployeeAllowanceId == alwid.EmployeeAllowanceId).SingleOrDefault();
                db.EmployeeAllowances.Remove(altyp);
            }

            var deduc = db.EmployeeDeductions.Where(x => x.EmployeeSalaryId == id).SingleOrDefault();
            if (deduc != null)
            {
                var deductyp = db.EmployeeDeductions.Where(x => x.EmployeeDedeuctionId == deduc.EmployeeDedeuctionId).SingleOrDefault();
                db.EmployeeDeductions.Remove(deductyp);
            }
            var levid = db.EmployeeLeaves.Where(x => x.EmployeeSalaryId == id).SingleOrDefault();
            if (levid != null)
            {
                var levidtyp = db.EmployeeLeaves.Where(x => x.EmployeeLeaveTypeId == levid.EmployeeLeaveTypeId).SingleOrDefault();
                db.EmployeeLeaves.Remove(levidtyp);
            }

            var emsal = db.EmployeeSalaries.Where(x => x.EmployeeSalaryId == id).SingleOrDefault();
            db.EmployeeSalaries.Remove(emsal);
            db.SaveChanges();

            EmployeeSalary obj = new EmployeeSalary();
            obj.BasicSelary = model.BasicSelary;
            obj.TotalAmount = model.TotalAmount;
            obj.CreatedBy = Convert.ToInt32(Session["EmployeeId"]);
            obj.CreatedAt = DateTime.Now;
            obj.EmployeeId = model.EmployeeId;
            db.EmployeeSalaries.Add(obj);
            db.SaveChanges();
            if (model.AllowanceTypeId != 0 && model.AllowanceTypeId > 0)
            {
                EmployeeAllowance obj1 = new EmployeeAllowance();
                obj1.AllowanceTypeId = model.AllowanceTypeId;
             
                obj1.AllowanceAmount = model.AllowanceAmount;
             
                obj1.EmployeeSalaryId = obj.EmployeeSalaryId;
                db.EmployeeAllowances.Add(obj1);
            }
            if (model.DeductionId != 0 && model.DeductionId > 0)
            {
                EmployeeDeduction obj2 = new EmployeeDeduction();
                obj2.DeductionAmount = model.DeductionAmount.GetValueOrDefault();
             
                obj2.EmployeeSalaryId = obj.EmployeeSalaryId;
                obj2.DeductionId = model.DeductionId;
              
                db.EmployeeDeductions.Add(obj2);
            }
            if (model.LeaveTypeId != 0 && model.LeaveTypeId > 0)
            {
                EmployeeLeave obj3 = new EmployeeLeave();
                obj3.EmployeeSalaryId = obj.EmployeeSalaryId;
                obj3.LeaveDays = model.LeaveDays;
                obj3.LeaveTypeId = model.LeaveTypeId;   
                db.EmployeeLeaves.Add(obj3);
            }
            db.SaveChanges();
            return View(model);
        }
        public ActionResult ListDelete(int? id)
        {
            var alwid = db.EmployeeAllowances.Where(x => x.EmployeeSalaryId == id).SingleOrDefault();
            if (alwid != null)
            {
                var altyp = db.EmployeeAllowances.Where(x => x.EmployeeAllowanceId == alwid.EmployeeAllowanceId).SingleOrDefault();
                db.EmployeeAllowances.Remove(altyp);
            }

            var deduc = db.EmployeeDeductions.Where(x => x.EmployeeSalaryId == id).SingleOrDefault();
            if (deduc != null)
            {
                var deductyp = db.EmployeeDeductions.Where(x => x.EmployeeDedeuctionId == deduc.EmployeeDedeuctionId).SingleOrDefault();
                db.EmployeeDeductions.Remove(deductyp);
            }
            var levid = db.EmployeeLeaves.Where(x => x.EmployeeSalaryId == id).FirstOrDefault();
            if (levid != null)
            {
                var levidtyp = db.EmployeeLeaves.Where(x => x.EmployeeLeaveTypeId == levid.EmployeeLeaveTypeId).FirstOrDefault();
                db.EmployeeLeaves.Remove(levidtyp);
            }

            var emsal = db.EmployeeSalaries.Where(x => x.EmployeeSalaryId == id).SingleOrDefault();
            db.EmployeeSalaries.Remove(emsal);
            db.SaveChanges();

            return RedirectToAction("list", "EmployeeSalary");
        }
    }
}
          

