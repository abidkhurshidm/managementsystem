﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using payrollado.Models;

namespace payrollado.Controllers
{
    public class LoginController : Controller
    {
        payrollEntities4 db = new payrollEntities4();
        // GET: Login
        public ActionResult Index()
        {
            if (Session["EmployeeId"] == null)
            {

                var res = db.UserTypes.ToList();
                ViewBag.UserTypeId = new SelectList(res, "UserTypeId", "UserTypeName");
                return View();
            }
            return RedirectToAction("Index", "Employee");
        }
        [HttpPost]
        public ActionResult Index(LoginSinup model)
        {
          
            using (var db = new payrollEntities4())
            {
                var res = db.UserTypes.ToList();
                ViewBag.UserTypeId = new SelectList(res, "UserTypeId", "UserTypeName");
                if (ModelState.IsValid)
                {
                    var pas = Cryptography.Encrypt(model.Password);  
                    var q = db.Users.Where(x => x.UserName == model.UserName && x.Password == pas).FirstOrDefault();
                    if (q != null)
                    {
                        //var password = q.Password;
                        //var apppas = Cryptography.Decrypt(password);
                        //if(pas == apppas)
                        //{
                            Session["UserTypeId"] = q.UserTypeId;
                            Session["EmployeeId"] = q.EmployeeId;
                            Session["Username"] = q.UserName;
                            return RedirectToAction("Index", "Employee", new { Empid = Session["EmployeeId"] });
                        }
                        else
                        {
                            ViewBag.Msg = "User Name or Password is in not valid";
                            return View("Index");
                        }
                }
                else
                {
                    return View();
                }
            }
        }
        public ActionResult Sinup()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Sinup(LoginSinup model)
        {
            using (var db = new payrollEntities4())
            {
                User obj = new User()
                {
                    UserName = model.UserName,
                    Password = Cryptography.Encrypt(model.Password)
                };
                db.Users.Add(obj);
                db.SaveChanges();
                return View();
            }
        }
    }
}