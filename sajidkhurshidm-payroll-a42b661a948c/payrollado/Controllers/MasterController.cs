﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using payrollado.Models;
namespace payrollado.Controllers
{
    public class MasterController : Controller
    {
        payrollEntities4 db = new payrollEntities4();
        // GET: Master  
        public ActionResult AddDepartment()
        {
            var query = db.Departments.ToList();
            List<SetupModel> list = new List<SetupModel>();
            if(query.Count != 0)
            {
                foreach (var item in query)
                {
                    SetupModel obj = new SetupModel();
                    obj.DepartmentId = item.DepartmentId;
                    obj.Name = item.Name;
                    list.Add(obj);
                }
                return View(list.ToList());
            }
            return View();
        }
        public ActionResult CreateDepartment()
        {   
            return PartialView("_Createdep", new SetupModel());
        }
        public ActionResult TableRendep()
        {
            return PartialView("_Tablerendep", new SetupModel());
        }
        public ActionResult Save(SetupModel model)
        {
            var msg = "";
            var res = db.Employees.ToList();
            ViewBag.EmployeeId = new SelectList(res, "EmployeeId", "FirstName");
            Department obj = new Department();
            var chk = db.Departments.Where(x => x.Name == model.Name).FirstOrDefault();
            if (chk != null)
            {
                msg = "exist";
                return Json(msg, JsonRequestBehavior.AllowGet);
            }
            
                obj.Name = model.Name;
                db.Departments.Add(obj);
                db.SaveChanges();
            msg = "save";
            return Json(msg, JsonRequestBehavior.AllowGet);
            
        }
        public ActionResult update(int id)
        {
            var res = db.Employees.ToList();
            ViewBag.EmployeeId = new SelectList(res, "EmployeeId", "FirstName");
            var slid = db.Departments.Where(x => x.DepartmentId == id).SingleOrDefault();
            SetupModel model = new SetupModel();
            model.Name = slid.Name;
            model.hidep = slid.DepartmentId;
            return PartialView("_Createdep", model);
        }
        [HttpGet]
        public ActionResult updatesave(SetupModel model, int id)
        {
            var msg = "";
            var chk = db.Departments.Where(x => x.Name == model.Name).FirstOrDefault();
            if (chk != null)
            {
                msg = "exist";
                return Json(msg, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var slid = db.Departments.Where(x => x.DepartmentId == id).SingleOrDefault();
                slid.Name = model.Name;
                msg = "update";
                db.SaveChanges();
                return Json(msg, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult DeleteDepartment(int id,SetupModel model)
        {
            var msg = "";
            var empid = db.Employees.Where(x => x.DepartmentId == id).FirstOrDefault();
            if (empid != null)
            {
                msg = "no";
                return Json(msg,JsonRequestBehavior.AllowGet);
            }
            else
            {
                IEnumerable<Designation> list = db.Designations.Where(x => x.DepartmentId == id).ToList();
                if (list != null)
                {
                    db.Designations.RemoveRange(list);
                }
                var depID = db.Departments.Where(x => x.DepartmentId == id).FirstOrDefault();
                db.Departments.Remove(depID);
                db.SaveChanges();
                msg = "Delete";
                return Json(msg, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult AddDesignation()
        {

            var res = db.Designations.ToList();
            ViewBag.fkDepartmentId = new SelectList(res, "Designation_Id", "Name");
            List<Designation> deslist = res;
            List<Department> deplist = db.Departments.ToList();
            var query = (from a in deslist
                         join b in deplist
                         on a.DepartmentId equals b.DepartmentId
                         select new
                         {
                             DesignationId = a.Designation_Id,
                             designationName = a.Name,  
                             DepartmentName =  b.Name
                         }
                         ).ToList();

            List<SetupModel> list = new List<SetupModel>();
            if (query.Count != 0)
            {
                foreach (var item in query)
                {
                    SetupModel obj = new SetupModel();
                    obj.Designation_Id = item.DesignationId;
                    obj.DesName = item.designationName;
                    obj.DepartmentName = item.DepartmentName;
                    list.Add(obj);
                }
                return View(list.ToList());
            }
            return View();
        }
        public ActionResult CreateDesignation()
        {
            var res = db.Departments.ToList();
            ViewBag.fkDepartmentId = new SelectList(res, "DepartmentId", "Name");
            return PartialView("_Createdes", new SetupModel());
        }
        public ActionResult TableRendes()
        {

            return PartialView("_Tablerendes", new SetupModel());
        }
        public ActionResult SaveDesignation(SetupModel model)
        {
            var msg = "";
            var res = db.Designations.ToList();
            ViewBag.fkDepartmentId = new SelectList(res, "Designation_Id", "Name");
            Designation obj = new Designation();
            var chk = db.Designations.Where(x => x.Name == model.DesName && x.DepartmentId == model.fkDepartmentId).FirstOrDefault();
            if (chk != null)
            {
                msg = "exist";
                return Json(msg, JsonRequestBehavior.AllowGet);
            }
            obj.Name = model.DesName;
            obj.DepartmentId = model.fkDepartmentId;
            db.Designations.Add(obj);
            db.SaveChanges();
            msg = "save";
            return Json(msg, JsonRequestBehavior.AllowGet);
        }
        public ActionResult updateDesignation(int id)
        {
            var res = db.Departments.ToList();
            ViewBag.fkDepartmentId = new SelectList(res, "DepartmentId", "Name");
            var slid = db.Designations.Where(x => x.Designation_Id == id).SingleOrDefault();
            SetupModel model = new SetupModel();
            model.DesName = slid.Name;
            model.fkDepartmentId = slid.DepartmentId.GetValueOrDefault();
            model.hides = slid.Designation_Id;
            return PartialView("_Createdes", model);
        }
        [HttpGet]
        public ActionResult updatesaveDesignation(SetupModel model, int id)
        {
            var msg = "";
            var res = db.Designations.ToList();
            ViewBag.fkDepartmentId = new SelectList(res, "Designation_Id", "Name");
            var chk = db.Designations.Where(x => x.Name == model.DesName && x.DepartmentId == model.DepartmentId).FirstOrDefault();
            if (chk != null)
            {
                msg = "exist";
                return Json(msg, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var slid = db.Designations.Where(x => x.Designation_Id == id).SingleOrDefault();
                slid.Name = model.DesName;
                db.SaveChanges();
                msg = "update";
                return Json(msg, JsonRequestBehavior.AllowGet);
            }
                

            
        }
        public ActionResult DeleteDesignation(int id, SetupModel model)
        {
            var msg = "";
            var empid = db.Employees.Where(x => x.Designation_Id == id).FirstOrDefault();
            if (empid != null)
            {
                msg = "no";
                return Json(msg, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var desID = db.Designations.Where(x => x.Designation_Id == id).FirstOrDefault();
                if (desID != null)
                {
                    db.Designations.Remove(desID);
                    db.SaveChanges();
                }
            }
            msg = "Delete";
            return Json(msg,JsonRequestBehavior.AllowGet);
        }
        //coment
        public ActionResult AddAllowance()
         {
            ViewBag.msg = TempData["data"];
            List<AllowanceType> alwlist = db.AllowanceTypes.ToList();
            List<EmployeeType> emplist = db.EmployeeTypes.ToList();
            var query = (from a in alwlist
                         join b in emplist
                         on a.EmployeeTypeId equals b.EmployeeTypeId
                         select new
                         {
                             AllowanceTypeI = a.AllowanceTypeId,
                             AllowanceType   = a.AllowanceType1 ,
                             EmployeeType = b.EmployeeType1,

                         }
                         ).ToList();

           
        List<SetupModel> list = new List<SetupModel>();
        if (query.Count != 0)
        {
            foreach (var item in query)
            {
                SetupModel obj = new SetupModel();
                obj.AllowancTypeid = item.AllowanceTypeI;
                obj.allowanceType = item.AllowanceType;
                obj.EmployeeTypeName = item.EmployeeType;
                list.Add(obj);
            }
            return View(list.ToList());
        }
        return View();
    }
    public ActionResult CreateAllowance()
    {
            var res1 = db.EmployeeTypes.ToList();
            ViewBag.EmployeeTypeId = new SelectList(res1, "EmployeeTypeId", "EmployeeType1");
            return PartialView("_Createalw", new SetupModel());
    }
    public ActionResult TableRendalw()
    {

        return PartialView("_Tablerenalw", new SetupModel());
    }
    public ActionResult SaveAllowance(SetupModel model)
    {
        var msg = "";
        var res1 = db.EmployeeTypes.ToList();
        ViewBag.EmployeeTypeId = new SelectList(res1, "EmployeeTypeId", "EmployeeType1");
        AllowanceType obj = new AllowanceType();
        var chk = db.AllowanceTypes.Where(x => x.AllowanceType1 == model.allowanceType && x.EmployeeTypeId == model.EmployeeTypeId).FirstOrDefault();
        if (chk != null)
        {
         msg = "exist";
         return Json(msg, JsonRequestBehavior.AllowGet);
        }
        obj.AllowanceType1 = model.allowanceType;
        obj.EmployeeTypeId = model.EmployeeTypeId;
        db.AllowanceTypes.Add(obj);
        db.SaveChanges();
        msg = "save";
        return Json(msg, JsonRequestBehavior.AllowGet);
    }
    public ActionResult updateAllowance(int? id)
    {
            var res1 = db.EmployeeTypes.ToList();
            ViewBag.EmployeeTypeId = new SelectList(res1, "EmployeeTypeId", "EmployeeType1");
            var slid = db.AllowanceTypes.Where(x => x.AllowanceTypeId == id).SingleOrDefault();
            SetupModel model = new SetupModel();
            model.allowanceType = slid.AllowanceType1;
            model.EmployeeTypeId = slid.EmployeeTypeId;
            model.hidalw = slid.AllowanceTypeId;
            return PartialView("_Createalw", model);
    }
    [HttpGet]
    public ActionResult updatesaveAllowance(SetupModel model, int id)
    {
            var msg = "";
           var res1 = db.EmployeeTypes.ToList();
            ViewBag.EmployeeTypeId = new SelectList(res1, "EmployeeTypeId", "EmployeeType1");
            var chk = db.AllowanceTypes.Where(x => x.AllowanceType1 == model.allowanceType && x.EmployeeTypeId == model.EmployeeTypeId).FirstOrDefault();
            if (chk != null)
            {
                msg = "exist";
                return Json(msg, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var slid = db.AllowanceTypes.Where(x => x.AllowanceTypeId == id).SingleOrDefault();
                slid.AllowanceType1 = model.allowanceType;
                slid.EmployeeTypeId = slid.EmployeeTypeId;
                db.SaveChanges();
                msg = "update";
                return Json(msg, JsonRequestBehavior.AllowGet);
            }
                
    }

        public ActionResult DeleteAllowance(int id, SetupModel model)
        {
            var msg = "";
            var empid = db.EmployeeAllowances.Where(x => x.AllowanceTypeId == id).FirstOrDefault();
            if (empid != null)
            {
                msg = "no";
                return Json(msg, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var alwtyp = db.AllowanceTypes.Where(x => x.AllowanceTypeId == id).FirstOrDefault(); 
                db.AllowanceTypes.Remove(alwtyp);
                db.SaveChanges();
            }
            
            msg = "delete";
            return Json(msg,JsonRequestBehavior.AllowGet);
        }
        public ActionResult AddLeaves()
        {
            ViewBag.msg = TempData["data"];
            var query = db.LeaveTypes.ToList();
            List<SetupModel> list = new List<SetupModel>();
            if (query.Count != 0)
            {
                foreach (var item in query)
                {
                    SetupModel obj = new SetupModel();
                    obj.LeaveTypeid = item.LeaveTypeId;
                    obj.LeaveType = item.LeaveName;
                    list.Add(obj);
                }
                return View(list.ToList());
            }
            return View();
        }
        public ActionResult CreateLeaves()
        {
            
            return PartialView("_Createlev", new SetupModel());
        }
        public ActionResult TableRendlev()
        {

            return PartialView("_Tablerenlev", new SetupModel());
        }
        public ActionResult SaveLeaves(SetupModel model)
        {
            var msg = "";
            LeaveType obj = new LeaveType();
            var chk = db.AllowanceTypes.Where(x => x.AllowanceType1 == model.allowanceType).FirstOrDefault();
            if (chk != null)
            {
                msg = "exist";
                return Json(msg, JsonRequestBehavior.AllowGet);
            }
            obj.LeaveName = model.LeaveType;
            db.LeaveTypes.Add(obj);
            db.SaveChanges();
            msg = "save";
            return Json(msg,JsonRequestBehavior.AllowGet);
        }
        public ActionResult updateLeaves(int? id)
        {
            var slid = db.LeaveTypes.Where(x => x.LeaveTypeId == id).SingleOrDefault();
            SetupModel model = new SetupModel();
            model.LeaveType = slid.LeaveName;
            model   .hidlev = slid.LeaveTypeId;
            return PartialView("_Createlev", model);
        }
        public ActionResult updatesaveLeaves(SetupModel model, int id)
        {
            var msg = "";
            
            var chk = db.LeaveTypes.Where(x => x.LeaveName == model.LeaveType).FirstOrDefault();
            if (chk != null)
            {
                msg = "exist";
                return Json(msg, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var slid = db.LeaveTypes.Where(x => x.LeaveTypeId == id).SingleOrDefault();
                slid.LeaveName = model.LeaveType;
                db.SaveChanges();
                msg = "update";
                return Json(msg, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult DeleteLeave(int id, SetupModel model)
        {
            var msg = "";
            var empid = db.EmployeeLeaves.Where(x => x.LeaveTypeId == id).FirstOrDefault();
            if (empid != null)
            {
                msg = "no";
                return Json(msg, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var levid = db.LeaveTypes.Where(x => x.LeaveTypeId == id).FirstOrDefault();
                db.LeaveTypes.Remove(levid);
                db.SaveChanges();
            }
            msg = "delete";
            return Json(msg,JsonRequestBehavior.AllowGet);
        }
        //public ActionResult Deleteleaves(int id, SetupModel model)
        //{
        //    var delev = db.LeaveTypes.Where(x => x.LeaveTypeId == id).FirstOrDefault();
        //    db.LeaveTypes.Remove(delev);
        //    db.SaveChanges();
        //    return RedirectToAction("AddLeaves", "Master");
        //}
        public ActionResult AddDeduction()
        {
            ViewBag.msg = TempData["data"];
            var query = db.Deductions.ToList();
            List<SetupModel> list = new List<SetupModel>();
            if (query.Count != 0)
            {
                foreach (var item in query)
                {
                    SetupModel obj = new SetupModel();
                    obj.DeductionId = item.DeductionId;
                    obj.DedeuctionName = item.DeductionName;
                    list.Add(obj);
                }
                return View(list.ToList());
            }
            return View();
        }
        public ActionResult CreateDeduction()
        {   
            return PartialView("_Createduc", new SetupModel());
        }
        public ActionResult TableRendduc()
        {
            return PartialView("_Tablerenduc", new SetupModel());
        }
        public ActionResult SaveDeduction(SetupModel model)
        {
            var msg = "";           
            Deduction obj = new Deduction();
            var chk = db.Deductions.Where(x => x.DeductionName == model.DedeuctionName).FirstOrDefault();
            if (chk != null)
            {
                msg = "exist";
                return Json(msg, JsonRequestBehavior.AllowGet);
            }
            obj.DeductionName = model.DedeuctionName;
            db.Deductions.Add(obj);
            db.SaveChanges();
            msg = "save";
            return Json(msg,JsonRequestBehavior.AllowGet);
        }
        public ActionResult updateDeduction(int? id)
        {
            var slid = db.Deductions.Where(x => x.DeductionId == id).SingleOrDefault();
            SetupModel model = new SetupModel();
            model.DedeuctionName = slid.DeductionName;
            model.hidduc = slid.DeductionId;
            return PartialView("_Createduc", model);
        }
        [HttpGet]
        public ActionResult updatesaveDeduction(SetupModel model, int id)
        {
            var msg = "";
            var chk = db.Deductions.Where(x => x.DeductionName == model.DedeuctionName).FirstOrDefault();
            if (chk != null)
            {
                msg = "exist";
                return Json(msg, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var slid = db.Deductions.Where(x => x.DeductionId == id).SingleOrDefault();
                slid.DeductionName = model.DedeuctionName;
                db.SaveChanges();
                msg = "update";
                return Json(msg, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult DeleteDeduction(int id, SetupModel model)
        {
            var msg = "";
            var empid = db.EmployeeDeductions.Where(x => x.DeductionId == id).FirstOrDefault();
            if (empid != null)
            {
                msg = "no";
                return Json(msg,JsonRequestBehavior.AllowGet);
            }
            else
            {
                var emp = db.Deductions.Where(x => x.DeductionId == id).FirstOrDefault();
                db.Deductions.Remove(emp);
                db.SaveChanges();
            }
            msg = "delete";
            return Json(msg,JsonRequestBehavior.AllowGet);
        }
        public ActionResult AddEmployee()
        {
            ViewBag.msg = TempData["data"];
            var query = db.EmployeeTypes.ToList();
            List<SetupModel> list = new List<SetupModel>();
            if (query.Count != 0)
            {
                foreach (var item in query)
                {
                    SetupModel obj = new SetupModel();
                    obj.EmployeeTypeId = item.EmployeeTypeId;
                    obj.EmployeeTypeName = item.EmployeeType1;
                    list.Add(obj);
                }
                return View(list.ToList());
            }
            return View();
        }
        public ActionResult CreateEmployee()
        {
            return PartialView("_Createemp", new SetupModel());
        }
        public ActionResult TableRendemp()
        {
            return PartialView("_Tablerenemp", new SetupModel());
        }
        public ActionResult SaveEmployee(SetupModel model)
        {
            var msg = "";
            EmployeeType obj = new EmployeeType();
            var chk = db.EmployeeTypes.Where(x => x.EmployeeType1 == model.EmployeeTypeName).FirstOrDefault();
            if (chk != null)
            {
                msg = "exist";
                return Json(msg, JsonRequestBehavior.AllowGet);
            }
            obj.EmployeeType1 = model.EmployeeTypeName;
            db.EmployeeTypes.Add(obj);
            db.SaveChanges();
            msg = "save";
            return Json(msg,JsonRequestBehavior.AllowGet);
        }
        public ActionResult updateEmployee(int? id)
        {
            var slid = db.EmployeeTypes.Where(x => x.EmployeeTypeId == id).SingleOrDefault();
            SetupModel model = new SetupModel();
            model.EmployeeTypeName = slid.EmployeeType1;
            model.hidemp = slid.EmployeeTypeId;
            return PartialView("_Createemp", model);
        }
        [HttpGet]
        public ActionResult updatesaveEmployee(SetupModel model, int id)
        {
            var msg = "";
            var chk = db.EmployeeTypes.Where(x => x.EmployeeType1 == model.EmployeeTypeName).FirstOrDefault();
            if (chk != null)
            {
                msg = "exist";
                return Json(msg, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var slid = db.EmployeeTypes.Where(x => x.EmployeeTypeId == id).SingleOrDefault();
                slid.EmployeeType1 = model.EmployeeTypeName;
                db.SaveChanges();
                msg = "update";
                return Json(msg, JsonRequestBehavior.AllowGet);
            }
         }
        public ActionResult DeleteEmployee(int id, SetupModel model)
        {
            var msg = "";
            var empid = db.Employees.Where(x => x.EmployeeTypeId == id).FirstOrDefault();
            if (empid != null)
            {
                msg = "no";
                return Json(msg,JsonRequestBehavior.AllowGet);
            }
            else
            {
                var emptyp = db.EmployeeTypes.Where(x => x.EmployeeTypeId == id).FirstOrDefault();
                db.EmployeeTypes.Remove(emptyp);
                db.SaveChanges();
            }
            msg = "delete";
            return Json(msg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddHoliday()
        {
            return View();
        }

        public JsonResult Getevent()
        {
            var events = db.Holidays.ToList().Select(s => new
            {
                s.HolidayId,
                s.HolidayTitle,
                s.Description,
                HolidayDateS = s.HolidayDateS.Value.ToString("dd/MMM/yyyy"),
                HolidayDateE = s.HolidayDateE != null ? s.HolidayDateE.Value.ToString("dd/MMM/yyyy") : null,
                s.Color,
                s.Fullday
            }).ToList();
            return new JsonResult { Data = events, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
        [HttpPost]
        public JsonResult SaveEvent(SetupModel model)
        {
            var status = false;
            Holiday obj = new Holiday();
                if (model.hdEventID > 0)
                {
                    //Update the event
                    var v = db.Holidays.Where(a => a.HolidayId == model.hdEventID).FirstOrDefault();
                    if (v != null)
                    {
                    v.HolidayTitle = model.title;
                    v.HolidayDateS = model.start;
                   // v.HolidayDateE = model.end;
                    v.Description = model.description;
                    v.Fullday = model.allday;
                    v.Color = model.color;
                    }
                }
                else
                    {
                obj.HolidayTitle = model.title;
                obj.HolidayDateS = model.start;
                obj.HolidayDateE = model.end;
                obj.Description = model.description;
                obj.Fullday = model.allday;
                obj.Color = model.color;
                db.Holidays.Add(obj);
                }
                db.SaveChanges();
                status = true;
            
            return new JsonResult { Data = new { status = status } };
        }
        [HttpPost]
        public JsonResult DeleteEvent(int holidayid)
        {
            var status = false;
            
                var v = db.Holidays.Where(a => a.HolidayId == holidayid).FirstOrDefault();
                if (v != null)
                {
                    db.Holidays.Remove(v);
                    db.SaveChanges();
                    status = true;
                }
            
            return new JsonResult { Data = new { status = status } };
        }
    }
}
