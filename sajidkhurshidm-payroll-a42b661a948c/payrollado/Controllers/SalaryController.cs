﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using payrollado.Models;
namespace payrollado.Controllers
{
    public class SalaryController : Controller
    {
        payrollEntities4 db = new payrollEntities4();
        // GET: Salary
        public ActionResult Index()
        {
            var res = db.Employees.ToList();
            ViewBag.EmployeeId = new SelectList(res, "EmployeeId", "FirstName");
            var query = db.multi().ToList(); 
            List<SalaryModel> list = new List<SalaryModel>();
            if (query.Count !=  0)
            {
                foreach (var item in query)
                {
                    SalaryModel obj = new SalaryModel();
                    obj.SalaryId = item.SalaryId;
                    obj.EmployeeId = item.EmployeeId;
                    obj.SalaryMonth = item.SalaryMonth.GetValueOrDefault();
                    obj.FirstName = item.FirstName;
                    obj.LastName = item.LastName;
                    obj.TotalAmount = item.TotalAmount.GetValueOrDefault();
                    list.Add(obj);
                }
                return View(list.ToList());
            }
            return View();
        }
        public ActionResult Create()
        {
            var res = db.Employees.ToList();
            ViewBag.EmployeeId = new SelectList(res, "EmployeeId", "FirstName");
            return PartialView("_Create", new SalaryModel());
        }
        public ActionResult TableRend()
        {
            var res = db.Employees.ToList();
            ViewBag.EmployeeId = new SelectList(res, "EmployeeId", "FirstName");
            return PartialView("_Tablerend", new SalaryModel() );
        }
        public ActionResult EmployeeId(SalaryModel model)
        {
            List<SalaryModel> list = new List<SalaryModel>();
            var res = db.Employees.ToList();
            ViewBag.EmployeeId = new SelectList(res, "EmployeeId", "FirstName");
            var res2 = db.res(model.EmployeeId).ToList();
            if (res2 != null)
            {
                foreach (var item in res2)
                {
                    SalaryModel obj = new SalaryModel();
                    obj.BasicSalary = item.BasicSelary.GetValueOrDefault();
                    obj.AllowanceAmount = item.AllowanceAmount.GetValueOrDefault();
                    obj.TotalAbsent = item.TotalAbsent.GetValueOrDefault();
                    obj.EmployeeTotalAmount = item.TotalAmount.GetValueOrDefault();
                    obj.AllowanceType = item.AllowanceType;
                    obj.DeductionName = item.DeductionName;
                    obj.DeductionAmount = item.DeductionAmount;
                }
                return Json(list.ToList(), JsonRequestBehavior.AllowGet);
            }
            return Json(JsonRequestBehavior.AllowGet);
        }
        public ActionResult Save(SalaryModel model)
        {
            var res = db.Employees.ToList();
            ViewBag.EmployeeId = new SelectList(res, "EmployeeId", "FirstName");
            Salary obj = new Salary();
            obj.EmployeeId = model.EmployeeId;
            obj.SalaryMonth=  model.SalaryMonth;
            obj.TotalAmount = model.NetAmount;
            db.Salaries.Add(obj);
            db.SaveChanges();
            var msg = "DATA HAS BEEN SAVE";
            return Json(msg, JsonRequestBehavior.AllowGet);
        }
        public ActionResult update(int id)
        {
            var res = db.Employees.ToList();
            ViewBag.EmployeeId = new SelectList(res, "EmployeeId", "FirstName");
            var res2 = db.res(id).FirstOrDefault();
            SalaryModel model = new SalaryModel();
            if (res2 != null)
            {
               
                model.EmployeeId = res2.EmployeeId;
                model.SalaryMonth = res2.SalaryMonth.GetValueOrDefault();
                model.NetAmount = res2.TotalAmount.GetValueOrDefault();
                model.BasicSalary = res2.BasicSelary.GetValueOrDefault();
                model.AllowanceType = res2.AllowanceType;
                model.TotalAbsent = res2.TotalAbsent.GetValueOrDefault();
                model.hid = res2.SalaryId.GetValueOrDefault();
               
            }
            return PartialView("_Create", model);
        }
        [HttpGet]
        public ActionResult updatesave(SalaryModel model, int id )
        {
            var res = db.Employees.ToList();
            ViewBag.EmployeeId = new SelectList(res, "EmployeeId", "FirstName");
            var slid = db.Salaries.Where(x => x.SalaryId == id).SingleOrDefault();
            slid.EmployeeId = model.EmployeeId;
            slid.SalaryMonth = model.SalaryMonth;
            slid.TotalAmount = model.NetAmount;
            db.SaveChanges();
            var msg = "Data Has Been Updated";
            return Json(msg, JsonRequestBehavior.AllowGet);
        }
        public ActionResult DeleteSalary(int id)
        {
            var msg = "";
            var salID = db.Salaries.Where(x => x.SalaryId == id).FirstOrDefault();
            if (salID != null)
            {
                db.Salaries.Remove(salID);
                db.SaveChanges();
            }
            msg = "delete";
            return Json(msg,JsonRequestBehavior.AllowGet);  
        }
    }
}
