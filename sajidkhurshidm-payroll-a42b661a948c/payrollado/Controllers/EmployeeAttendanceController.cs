﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using payrollado.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Web.Script.Serialization;
using System.IO;
namespace payrollado.Controllers    
{
    public class EmployeeAttendanceController : Controller
    {
        payrollEntities4 db = new payrollEntities4();
        // GET: EmployeeAttendance
        public ActionResult Index()
        {
            var res = db.EmployeeTypes.ToList();
            ViewBag.EmployeeTypeId = new SelectList(res, "EmployeeTypeId", "EmployeeType1");
            return View();
        }
        [HttpPost]
        public ActionResult Index(AttendanceModel model)
        {
            var res = db.EmployeeTypes.ToList();
            ViewBag.EmployeeTypeId = new SelectList(res, "EmployeeTypeId", "EmployeeType1");
            var data = db.GetAttendance(model.AttendanceDatef, model.AttendanceDatet).ToList();
            model.attendances = data;
            return View(model);
        }
        public ActionResult leave(int? id )
        { 
            var res = db.LeaveTypes.ToList();
            ViewBag.LeaveTypeId = new SelectList(res, "LeaveTypeId", "LeaveName");
            var res3 = db.Employees.ToList();
            ViewBag.EmployeeId = new SelectList(res3, "EmployeeId", "FirstName");
            if(id.HasValue)
            {
                AttendanceModel model = new AttendanceModel();
                var levid = db.Leaves.Where(x => x.LeaveId == id).FirstOrDefault();
                model.LeaveTypeId = levid.LeaveTypeId;
                model.EmployeeId = levid.EmployeeId;
                model.AttendanceDatef = levid.Fromdate.Value.Date;
                model.AttendanceDatet = levid.Todate.Value.Date;
                model.Reason = levid.Reason;
                model.Attachment = levid.Attachment;
                model.levhid = levid.LeaveId;
                return View(model);
            }
            return View();
        }
        [HttpPost]
        public ActionResult leave(AttendanceModel model)
        {
            
            var res3 = db.Employees.ToList();
            ViewBag.EmployeeId = new SelectList(res3, "EmployeeId", "FirstName");
            var res4 = db.LeaveTypes.ToList();
            ViewBag.LeaveTypeId = new SelectList(res4, "LeaveTypeId", "LeaveName");
            Leave obj = new Leave();
            var dtjoin = db.Employees.Where(x => x.EmployeeId == model.EmployeeId).FirstOrDefault().DateofJoining;
            int leaveid = 3;
            var id = model.levhid;
            double totaldays = model.AttendanceDatet.Value.Subtract(model.AttendanceDatef.Value).TotalDays + 1;
            var sub = "";
            var no = "";
            var date = "";
            if (id != 0)
            {
                if (dtjoin < model.AttendanceDatef)
                {
                    var list = db.Attendances.Where(x => x.LeaveId == model.levhid).ToList();
                    db.Attendances.RemoveRange(list);
                    db.SaveChanges();

                    var levid = db.Leaves.Where(x => x.LeaveId == model.levhid).FirstOrDefault();
                    //db.Leaves.Remove(levchk);
                    //db.SaveChanges();

                    List<Attendance> lists = db.Attendances.ToList();
                    var leavegotup = lists.Where(x => x.LeaveTypeid == model.LeaveTypeId && x.Markattendance == 3).Count();
                    var leavegetup = model.leaveDeduct;
                    var totalleaveup = model.leaveDays;
                    var sumup = leavegotup + leavegetup;
                    var resup = totalleaveup - sumup;

                    double totaldaysup = model.AttendanceDatet.Value.Subtract(model.AttendanceDatef.Value).TotalDays + 1;
                    if (resup >= 0)
                    {
                        string _FileName = "";
                        if (model.AttachmentFile != null)
                        {
                            _FileName = Path.GetFileNameWithoutExtension(model.AttachmentFile.FileName);
                            string extention = Path.GetExtension(model.AttachmentFile.FileName);
                            _FileName = _FileName + extention;
                            model.AttachmentFile.SaveAs(Path.Combine(Server.MapPath("~/PDF/"), _FileName));
                        }
                        levid.LeaveTypeId = model.LeaveTypeId;
                        levid.Reason = model.Reason;
                        levid.Attachment = _FileName;
                        levid.Fromdate = model.AttendanceDatef;
                        levid.Todate = model.AttendanceDatet;
                        levid.EmployeeId = model.EmployeeId;
                        db.SaveChanges();
                        IEnumerable<Attendance> list2 = db.Attendances.Where(x => x.AttendanceDate >= model.AttendanceDatef && x.AttendanceDate <= model.AttendanceDatet && x.EmployeeId == model.EmployeeId).ToList();
                        if (list2 != null)
                        {
                            db.Attendances.RemoveRange(list2);
                            db.SaveChanges();
                        }
                        for (var i = 0; i < totaldays; i++)
                        {
                            Attendance obj2 = new Attendance();
                            obj2.AttendanceDate = model.AttendanceDatef.Value.AddDays(i);
                            obj2.EmployeeId = model.EmployeeId;
                            obj2.Markattendance = leaveid;
                            obj2.CreatedAt = DateTime.Now;
                            obj2.LeaveId = id;
                            obj2.LeaveTypeid = model.LeaveTypeId;
                            db.Attendances.Add(obj2);
                        }
                        db.SaveChanges();
                        //var atdchk = db.Attendance.Where(x=> x.AttendanceId ==)
                        sub = "submitted";
                        return Json(sub, JsonRequestBehavior.AllowGet);
                    }
                    no = "No";
                    return Json(no, JsonRequestBehavior.AllowGet);
                }
                date = "Date";
                return Json(date, JsonRequestBehavior.AllowGet);
            }

            else
            {

                var totalleave = model.leaveDays;
                var leavegot = model.rleaves;
                var leaveget = model.leaveDeduct;
                var sum = leavegot + leaveget;
                var res = totalleave - sum;
                var atdte = model.AttendanceDatef;

                if (dtjoin < model.AttendanceDatef)
                {
                    if (res >= 0)
                    {
                        string _FileName = "";
                        if (model.AttachmentFile != null)
                        {
                            _FileName = Path.GetFileNameWithoutExtension(model.AttachmentFile.FileName);
                            string extention = Path.GetExtension(model.AttachmentFile.FileName);
                            _FileName = _FileName + extention;
                            model.AttachmentFile.SaveAs(Path.Combine(Server.MapPath("~/PDF/"), _FileName));
                        }
                        obj.LeaveTypeId = model.LeaveTypeId;
                        obj.Reason = model.Reason;
                        obj.Attachment = _FileName;
                        obj.Fromdate = model.AttendanceDatef;
                        obj.Todate = model.AttendanceDatet;
                        obj.EmployeeId = model.EmployeeId;
                        db.Leaves.Add(obj);
                        db.SaveChanges();

                        IEnumerable<Attendance> list = db.Attendances.Where(x => x.AttendanceDate >= model.AttendanceDatef && x.AttendanceDate <= model.AttendanceDatet && x.EmployeeId == model.EmployeeId).ToList();
                        if (list != null)
                        {
                            db.Attendances.RemoveRange(list);
                            db.SaveChanges();
                        }
                       
                        for (var i = 0; i < totaldays; i++)
                        {
                            Attendance obj2 = new Attendance();
                            obj2.AttendanceDate = model.AttendanceDatef.Value.AddDays(i);
                            obj2.EmployeeId = model.EmployeeId;
                            obj2.Markattendance = leaveid;
                            obj2.CreatedAt = DateTime.Now;
                            obj2.LeaveId = obj.LeaveId;
                            obj2.LeaveTypeid = model.LeaveTypeId;
                            db.Attendances.Add(obj2);
                        }
                        db.SaveChanges();
                        //var atdchk = db.Attendance.Where(x=> x.AttendanceId ==)
                        sub = "submitted";
                        return Json(sub, JsonRequestBehavior.AllowGet);
                    }
                    no = "No";
                    return Json(no, JsonRequestBehavior.AllowGet);
                }
                
            }
            date = "Date";
            return Json(date, JsonRequestBehavior.AllowGet);
        }
                public ActionResult LeaveDelete(int id, int? EmpId, AttendanceModel model)
                {
                    IEnumerable<Attendance> list = db.Attendances.Where(x => x.LeaveId == id ).ToList();
                    db.Attendances.RemoveRange(list);
                    db.SaveChanges();
                    var levid = db.Leaves.Where(x => x.LeaveId == id).FirstOrDefault();
                    db.Leaves.Remove(levid);
                    db.SaveChanges();
                    return RedirectToAction("list", "EmployeeAttendance");
                }
        [HttpPost]
        public JsonResult jsres(List<Attenanceinfo> model)
        {
            foreach (var row in model)
            {
                foreach (var att in row.Employees)
                {
                    Attendance attendance = db.Attendances.Where(w => w.AttendanceId == att.attid).FirstOrDefault();
                    if (attendance == null)
                    {
                        attendance = new Attendance();
                        db.Attendances.Add(attendance);
                    }
                    attendance.AttendanceDate = DateTime.Parse(row.AttDate);
                    attendance.EmployeeId = att.empid;
                    attendance.Markattendance = att.mark;
                    attendance.CreatedAt = DateTime.Now;
                }
            }
            db.SaveChanges();
            //AttendanceModel item = JsonConvert.DeserializeObject<json>(jsonresult.ToString());
            //var logJson = JsonConvert.SerializeObject(table);
            return Json(model, JsonRequestBehavior.AllowGet);
        }
        public ActionResult list(AttendanceModel model)
        {
            var res = db.leavesp();
            return View(res);
        }
       // public ActionResult listupdated(int? id, AttendanceModel model)
       // {
       //     var res = db.LeaveTypes.ToList();
       //     ViewBag.LeaveTypeId = new SelectList(res, "LeaveTypeId", "LeaveName");
       //     var res3 = db.Employees.ToList();
       //     ViewBag.EmployeeId = new SelectList(res3, "EmployeeId", "FirstName");
       //     var livid = db.Leaves.Where(x => x.LeaveId == id).SingleOrDefault();
       //     model.LeaveTypeId = livid.LeaveTypeId;
       //     model.Reason = livid.Reason;
       //     model.AttendanceDatef = livid.Fromdate;
       //     model.AttendanceDatet = livid.Todate;
       //     model.Attachment = livid.Attachment;
       //     model.EmployeeId = livid.EmployeeId;
       //     model.hid = id.GetValueOrDefault();
       //     return View(model);
       // }
       // public ActionResult DataUpdated(AttendanceModel model)
       // {
       //     int leaveid = 3;
       //     int present = 1;
       //     int absent = 2;
       //     var totalleave = model.leaveDays;
       //     var leavegot = model.rleaves;
       //     var leaveget = model.leaveDeduct;
       //     var sum = leavegot + leaveget;
       //     var res = totalleave - sum;
       //     var atdte = model.AttendanceDatef;
       //     var result = "you Have No More leaves";
       //     double totaldays = model.AttendanceDatet.Value.Subtract(model.AttendanceDatef.Value).TotalDays + 1;
       //     if (res >= 0)
       //     {
       //       string _FileName = "";
       //     if (model.AttachmentFile != null)
       //     {
       //         _FileName = Path.GetFileNameWithoutExtension(model.AttachmentFile.FileName);
       //         string extention = Path.GetExtension(model.AttachmentFile.FileName);
       //         _FileName = _FileName + extention;
       //         model.AttachmentFile.SaveAs(Path.Combine(Server.MapPath("~/PDF/"), _FileName));
            
       //     }
       //     var lvid = db.Leaves.Where(x => x.LeaveId == model.hid).SingleOrDefault();
       //     lvid.LeaveTypeId = model.LeaveTypeId;
       //     lvid.Reason = model.Reason;
       //     lvid.Fromdate = model.AttendanceDatef;
       //     lvid.Todate = model.AttendanceDatet;
       //     lvid.EmployeeId = model.EmployeeId;
       //     lvid.Attachment = _FileName;
       //     db.SaveChanges();
       //     IEnumerable<Attendance> list = db.Attendances.Where(x => x.LeaveId == model.hid).ToList();
       //     db.Attendances.RemoveRange(list);
       //     db.SaveChanges();   
       //     for (var i = 0; i < totaldays; i++)
       //     {
       //             Attendance obj2 = new Attendance();
       //             obj2.AttendanceDate = model.AttendanceDatef.Value.AddDays(i);
       //             obj2.EmployeeId = model.EmployeeId;
       //             obj2.Markattendance = leaveid;
       //             obj2.CreatedAt = DateTime.Now;
       //             obj2.LeaveId = model.LeaveId;
       //             db.Attendances.Add(obj2);
       //     }
       //         db.SaveChanges();
       //        var mes = "Your data Update";
       //         return Json(result, JsonRequestBehavior.AllowGet);
       //     }
       //     return Json(result, JsonRequestBehavior.AllowGet);
       //}
      
        public ActionResult LeaveTypeId(int? LeaveTypeId)
        {      
            return Json(db.EmployeeLeaves.Where(w => w.LeaveTypeId == LeaveTypeId).Select(s => new { days = s.LeaveDays }).FirstOrDefault(), JsonRequestBehavior.AllowGet);                                     
        }
        public ActionResult attenadce(int? LeaveTypeId)
        {
            List<Attendance> list = db.Attendances.ToList();
            var count = list.Where(x => x.LeaveTypeid == LeaveTypeId && x.Markattendance == 3).Count();
            return Json(count, JsonRequestBehavior.AllowGet);
        }
       
    }
}
