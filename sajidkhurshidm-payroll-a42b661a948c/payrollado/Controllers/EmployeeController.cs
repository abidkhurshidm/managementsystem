﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using payrollado.Models;
using Newtonsoft.Json;
using System.Data.Entity;
namespace payrollado.Controllers
{ 
    public class EmployeeController : Controller
    {
        payrollEntities4 db = new payrollEntities4();
        

        public ActionResult Index(int? id)
        {
            EmployeeModal modal = new EmployeeModal();
            using (var db = new payrollEntities4())
            {
                
                if (Session["EmployeeId"] != null)
                {
                    modal = new EmployeeModal();
                    var res = db.Departments.ToList();
                    ViewBag.DepartmentId = new SelectList(res, "DepartmentId", "Name");
                    var res2 = db.Employees.ToList();
                    ViewBag.EmployeeId = new SelectList(res2, "EmployeeId", "FirstName");
                    var res5 = db.EmployeeTypes.ToList();
                    ViewBag.EmployeeTypeId = new SelectList(res5, "EmployeeTypeId", "EmployeeType1");      
                    ViewBag.Designation_Id = new SelectList("");
                    //modal.DateofBirth = null;

                    if (id.HasValue)
                    {
                        var res3 = db.EmployeeTypes.ToList();
                        var res4 = db.Designations.ToList();   
                        var empid = db.Employees.Where(x => x.EmployeeId == id).FirstOrDefault();
                        ViewBag.Designation_Id = new SelectList(res4, "Designation_Id", "Name", empid.Designation_Id);
                        modal.hid = id.GetValueOrDefault();
                        modal.FirstName = empid.FirstName;
                        modal.LastName = empid.LastName;
                        modal.Email = empid.Email;
                        modal.Address = empid.Address;
                        modal.ContactNO = empid.ContactNO;
                        modal.NIC = empid.NIC;
                        modal.DateofBirth = empid.DateofBirth.GetValueOrDefault();
                        modal.DateofJoining = empid.DateofJoining;
                        modal.DepartmentId = empid.DepartmentId;
                        modal.Designation_Id = empid.Designation_Id;
                        modal.IsActive = empid.IsActive.GetValueOrDefault();
                        modal.DepartmentName = empid.DepartmentName;
                        modal.DesignationName = empid.DesignationName;
                        modal.Gender = empid.Gender;
                        modal.EmployeeTypeId = empid.EmployeeTypeId.GetValueOrDefault();

                        var empeduchk = db.EmployeeEducations.Where(w => w.Employeeid == id).FirstOrDefault().EmployeeEducationId;
                        var empedu = db.EmployeeEducations.Where(w => w.EmployeeEducationId == empeduchk).FirstOrDefault();
                        modal.LastDegree = empedu.LastDegree;
                        modal.Institute = empedu.Institute;
                        modal.Grade = empedu.Grade;
                        modal.Division = empedu.Division;
                        modal.Percentage = empedu.Percentage;
                        modal.PassingYear = empedu.PassingYear;
                        modal.CreatedBY = Convert.ToInt32(Session["EmployeeId"]);
                        modal.CreatedAt = DateTime.Now;
                        modal.EmployeeId = empedu.Employeeid.GetValueOrDefault();
                        return View(modal);
                    }
                    else
                    {
                        return View(modal);                        
                    }
                }
                    return RedirectToAction("Index", "Login");
            }
        }
        [HttpPost]
        public ActionResult Index(EmployeeModal modal)
        {

            var res = db.Departments.ToList();
            ViewBag.DepartmentId = new SelectList(res, "DepartmentId", "Name");
            var res1 = db.EmployeeTypes.ToList();
            ViewBag.EmployeeTypeId = new SelectList(res1, "EmployeeTypeId", "EmployeeType1");
            ViewBag.Designation_Id = new SelectList("");

            using (var db = new payrollEntities4())
            {
                int age = DateTime.Now.Year - modal.DateofBirth.Value.Year;
                if (age < 18)
                {
                    var msg = "Agevalid";
                    return Json(msg, JsonRequestBehavior.AllowGet);
                }
                var date = DateTime.Today;
                var date2 = modal.DateofJoining.Value.Date;
                if (date2 > date)
                {
                    var msg = "datejoin";
                    return Json(msg, JsonRequestBehavior.AllowGet);
                }
                var id = modal.hid;

                if (id == 0)
                {
                    Employee el = new Employee()
                    {
                        FirstName = modal.FirstName,
                        LastName = modal.LastName,
                        Email = modal.Email,
                        Address = modal.Address,
                        ContactNO = modal.ContactNO,
                        NIC = modal.NIC,
                        DateofBirth = modal.DateofBirth,
                        DateofJoining = modal.DateofJoining,
                        DepartmentId = modal.DepartmentId,
                        Designation_Id = modal.Designation_Id,
                        CreatedBY = Convert.ToInt32(Session["EmployeeId"]),
                        CreatedAt = DateTime.Now,
                        IsActive = modal.IsActive,
                        DepartmentName = modal.DepartmentName,
                        Gender = modal.Gender,
                        EmployeeTypeId = modal.EmployeeTypeId,
                    };
                    db.Employees.Add(el);
                    EmployeeEducation edu = new EmployeeEducation()
                    {
                        LastDegree = modal.LastDegree,
                        Institute = modal.Institute,
                        Grade = modal.Grade,
                        Division = modal.Division,
                        Percentage = modal.Percentage,
                        PassingYear = modal.PassingYear,
                        CreatedBy = Convert.ToInt32(Session["EmployeeId"]),
                        CreatedAt = DateTime.Now,
                        Employeeid = el.EmployeeId,
                    };
                    db.EmployeeEducations.Add(edu);
                    db.SaveChanges();
                    var msg3 = "Save";
                    return Json(msg3, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var empedu = db.Employees.Where(w => w.EmployeeId == id).FirstOrDefault();
                    empedu.FirstName = modal.FirstName;
                    empedu.LastName = modal.LastName;
                    empedu.Email = modal.Email;
                    empedu.Address = modal.Address;
                    empedu.ContactNO = modal.ContactNO;
                    empedu.NIC = modal.NIC;
                    empedu.DateofBirth = modal.DateofBirth;
                    empedu.DateofJoining = modal.DateofJoining;
                    empedu.DepartmentId = modal.DepartmentId;
                    empedu.Designation_Id = modal.Designation_Id;
                    empedu.CreatedBY = Convert.ToInt32(Session["EmployeeId"]);
                    empedu.CreatedAt = DateTime.Now;
                    empedu.IsActive = modal.IsActive;
                    empedu.DepartmentName = modal.DepartmentName;
                    empedu.Gender = modal.Gender;
                    empedu.EmployeeTypeId = modal.EmployeeTypeId;

                    var empeduid = db.EmployeeEducations.Where(w => w.Employeeid == modal.hid).FirstOrDefault().EmployeeEducationId;
                    var empchk = db.EmployeeEducations.Where(w => w.EmployeeEducationId == empeduid).FirstOrDefault();
                    empchk.LastDegree = modal.LastDegree;
                    empchk.Institute = modal.Institute;
                    empchk.Grade = modal.Grade;
                    empchk.Division = modal.Division;
                    empchk.Percentage = modal.Percentage;
                    empchk.PassingYear = modal.PassingYear;
                    empchk.CreatedBy = Convert.ToInt32(Session["EmployeeId"]);
                    empchk.CreatedAt = DateTime.Now;
                    empchk.Employeeid = modal.hid;
                    db.SaveChanges();
                    
                }

            }
            var msg4 = "Update";
            return Json(msg4, JsonRequestBehavior.AllowGet);
        }
        
        [HttpGet]
        public JsonResult departmentID(int departmentid)
        {
            return Json(db.Designations.Where(x => x.DepartmentId == departmentid).Select(x => new
            {
                id = x.Designation_Id,
                name = x.Name
            }).ToList(), JsonRequestBehavior.AllowGet);
        }
        //public JsonResult EmployeeTypeId(int EmployeeTypeId)
        //{


        //    //using (var db = new payrollEntities2())
        //    //{
        //    //    db.Configuration.ProxyCreationEnabled = false;
        //    //    List<Employee> list = db.Employees.Where(x => x.EmployeeTypeId == EmployeeTypeId).Select(x => x.EmployeeType).ToList();            
        //    //    return Json(list, JsonRequestBehavior.AllowGet);
        //    //}
        //}
        public ActionResult Authorize()
        {
            var data = db.Users.ToList();
            var res = db.Employees.ToList();
            ViewBag.EmployeeId = new SelectList(res, "EmployeeId", "FirstName");
            var res1 = db.UserTypes.ToList();
            ViewBag.UserTypeId = new SelectList(res1, "UserTypeId", "UserTypeName");
            var mymodal = new LoginSinup();
            mymodal.Users = db.Users.ToList();
            return View(mymodal);
        }
        [HttpPost]
        public ActionResult Authorize(LoginSinup model)
        {
            var res = db.Employees.ToList();
            ViewBag.EmployeeId = new SelectList(res, "EmployeeId", "FirstName");
            var res1 = db.UserTypes.ToList();
            ViewBag.UserTypeId = new SelectList(res1, "UserTypeId", "UserTypeName");
            
            User obj = new User();
            obj.EmployeeId = model.EmployeeId;
            obj.UserTypeId = model.UserTypeId;
            obj.UserName = model.UserName;
            obj.UserTypeName = model.UserTypeName;
            obj.PasswordN = model.PasswordNor;
            obj.Password = Cryptography.Encrypt(model.Password);
            db.Users.Add(obj);
            db.SaveChanges();
            var mymodal = new LoginSinup();
            mymodal.Users = db.Users.ToList();
            return View(mymodal);
        }
        [HttpGet]
        public ActionResult Authorizeupdate(int id, LoginSinup model)
        {
            var res = db.Employees.ToList();
            ViewBag.EmployeeId = new SelectList(res, "EmployeeId", "FirstName");
            var res1 = db.UserTypes.ToList();
            ViewBag.UserTypeId = new SelectList(res1, "UserTypeId", "UserTypeName");
            var usid = db.Users.Where(x => x.UserId == id).FirstOrDefault();
            model.EmployeeId = usid.EmployeeId.GetValueOrDefault();
            model.UserTypeId = usid.UserTypeId.GetValueOrDefault();
            model.UserName = usid.UserName;
            model.hid = usid.UserId;
            model.Password = Cryptography.Decrypt(usid.Password);
            db.SaveChanges();
            return View(model);
        }
        [HttpGet]
        public ActionResult Authorizeupdatesave(int id , LoginSinup model)
        {
            var usid = db.Users.Where(x => x.UserId == id).FirstOrDefault();
            usid.EmployeeId = model.EmployeeId;
            usid.UserTypeId = model.UserTypeId;
            usid.UserTypeName = model.UserTypeName;
            usid.UserName = model.UserName;
            usid.PasswordN = model.PasswordNor;
            usid.Password = Cryptography.Encrypt(model.Password);
            db.SaveChanges();
            return Json("Success", JsonRequestBehavior.AllowGet);
        }
        public ActionResult Sess()
        {
            Session.Abandon();
            return RedirectToAction("Index", "Login");
        }
        [HttpGet]
        public ActionResult name(string name)
        {
            var chknme = db.Users.Where(x => x.UserName.Contains(name)).Select(s => new
            {
                Name = s.UserName
            });
            return Json(chknme, JsonRequestBehavior.AllowGet);
        }
        public ActionResult list()
       { 
            using (payrollEntities4 obj = new payrollEntities4())
            {
                ViewBag.Msg = TempData["data"];
                var mymodel = new Modal();
                mymodel.employees = obj.Employees.ToList();
                mymodel.employeeEducations = obj.EmployeeEducations.ToList();
                return View(mymodel);
            }
        }
        public ActionResult listupdate(int? id, EmployeeModal modal)
        {
            using (var db = new payrollEntities4())
            {
                var res = db.Departments.ToList();
                ViewBag.DepartmentId = new SelectList(res, "DepartmentId", "Name");
                var res1 = db.EmployeeTypes.ToList();
                ViewBag.EmployeeTypeId = new SelectList(res1, "EmployeeTypeId", "EmployeeType1");
                var res2 = db.Designations.ToList();
                ViewBag.Designation_Id = new SelectList(res2, "Designation_Id", "Name");

                var Empid = db.Employees.Where(x => x.EmployeeId == id).FirstOrDefault();

                modal.FirstName = Empid.FirstName;
                modal.LastName = Empid.LastName;
                modal.Email = Empid.Email;
                modal.Address = Empid.Address;
                modal.ContactNO = Empid.ContactNO;
                modal.NIC = Empid.NIC;
                modal.DateofBirth = Empid.DateofBirth;
                modal.DateofJoining = Empid.DateofJoining;
                modal.DepartmentId = Empid.DepartmentId;
                modal.Designation_Id = Empid.Designation_Id;
                modal.IsActive = Empid.IsActive.GetValueOrDefault();
                modal.DepartmentName = Empid.DepartmentName;
                modal.DesignationName = Empid.DesignationName;
                modal.Gender = Empid.Gender;
                modal.EmployeeTypeId = Empid.EmployeeTypeId.GetValueOrDefault();

                var Empedu = db.EmployeeEducations.Where(x => x.Employeeid == id).SingleOrDefault();               
                //var Empedu = db.EmployeeEducations.Where(x => x.EmployeeEducationId == Empid.).FirstOrDefault;
                modal.LastDegree = Empedu.LastDegree;
                modal.Institute = Empedu.Institute;
                modal.Grade = Empedu.Grade;
                modal.Division = Empedu.Division;
                modal.Percentage = Empedu.Percentage;
                modal.PassingYear = Empedu.PassingYear;
                modal.EmployeeIdfk = Empedu.Employeeid.GetValueOrDefault();
                modal.CreatedBY = Convert.ToInt32(Session["EmployeeId"]);
                modal.CreatedAt = DateTime.Now;
                modal.EmployeeId = Empedu.Employeeid.GetValueOrDefault();
                modal.EmployeeEducationId = Empedu.EmployeeEducationId;
                return View(modal);
            }
        }
        [HttpPost]
        public ActionResult listupdate( EmployeeModal modal)
        {
            using (payrollEntities4 db = new payrollEntities4())
            {
                var res2 = db.Designations.ToList();
                ViewBag.Designation_Id = new SelectList(res2, "Designation_Id", "Name");
                var res = db.Departments.ToList();
                ViewBag.DepartmentId = new SelectList(res, "DepartmentId", "Name");
                var res1 = db.EmployeeTypes.ToList();
                ViewBag.EmployeeTypeId = new SelectList(res1, "EmployeeTypeId", "EmployeeType1");
                
                var empid = db.Employees.Where(x => x.EmployeeId == modal.EmployeeId).FirstOrDefault();
                
                empid.FirstName = modal.FirstName;
                empid.LastName = modal.LastName;
                empid.Email = modal.Email;
                empid.Address = modal.Address;
                empid.ContactNO = modal.ContactNO;
                empid.NIC = modal.NIC;
                empid.DateofBirth = modal.DateofBirth;
                empid.DateofJoining = modal.DateofJoining;
                empid.DepartmentId = modal.DepartmentId;
                empid.Designation_Id = modal.Designation_Id;
                empid.IsActive = modal.IsActive;
                empid.Gender = modal.Gender;
                empid.EmployeeTypeId = modal.EmployeeTypeId;
                db.SaveChanges();
            
                var empedu = db.EmployeeEducations.Where(x => x.EmployeeEducationId == modal.EmployeeEducationId).FirstOrDefault();

                empedu.LastDegree = modal.LastDegree;
                empedu.Institute = modal.Institute;
                empedu.Grade = modal.Grade;
                empedu.Division = modal.Division;
                empedu.Percentage = modal.Percentage;
                empedu.PassingYear = modal.PassingYear;
                empedu.Employeeid = modal.EmployeeId;
                empedu.ModifiedBy = Convert.ToInt32(Session["EmployeeId"]);
                empedu.ModifiedDate = DateTime.Now;
                
                db.SaveChanges();
            }
            return View();
        }
        //public JsonResult AllowanceTypeid(int? AllowanceTypeId)
        //{
        //    using (var db = new payrollEntities3())
        //    {
        //        var empid = db.AllowanceTypes.Where(x => x.AllowanceTypeId == AllowanceTypeId).Select(x => new
        //        {
        //            amount = x.AlloawnceAmount
        //        });
        //       var json  =  JsonConvert.SerializeObject(empid);               
        //        return Json(json, JsonRequestBehavior.AllowGet);
        //    }
        //}

       

        public ActionResult ListDelete(int? id )
        {
            var chk = db.Employees.Where(x => x.EmployeeId == id).Include(es => es.EmployeeSalaries).Include(at => at.Attendances).Include(em => em.EmployeeType).Include(lev => lev.Leaves).Include(sal => sal.Salaries).ToList();
            if (chk != null)
            {
                TempData["data"] = "This Record Cannot Be Deleted ";
            }
            else
            {
                var Empid = db.Employees.Where(x => x.EmployeeId == id).FirstOrDefault();
                var empedu = db.EmployeeEducations.Where(x => x.EmployeeEducationId == Empid.EmployeeId).FirstOrDefault();
                db.EmployeeEducations.Remove(empedu);
                db.Employees.Remove(Empid);
                db.SaveChanges();
            } 
        
               return RedirectToAction("list","Employee");
        }
    }
}

 
    

   

    
    
