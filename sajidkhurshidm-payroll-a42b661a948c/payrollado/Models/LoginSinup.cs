﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace payrollado.Models

{
    public class LoginSinup
    {
        public int UserId { get; set; }
        [Required(ErrorMessage ="User Name Must Required Here")]
        public string UserName { get; set; }

        public int UserTypeId { get; set; }
        [Required(ErrorMessage = "Please Insert Password here")]
        [DataType(DataType.Password)]

        public string Password { get; set; }

        public string ConfirmPassword { get; set; }

        public int EmployeeId { get; set; }

        public string EmployeeIdName { get; set; }

        public int CreatedBy { get; set; }

        public DateTime CreatedAt { get; set; }

        public int ModifiedBy { get; set; }

        public DateTime ModifiedAt { get; set; }

        public int hid { get; set; }

        public IEnumerable<User> Users { get; set; }

        public string UserTypeName { get; set; }

        public string PasswordNor { get; set; }
    }
}