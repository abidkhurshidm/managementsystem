﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace payrollado.Models
{
    public class SalaryModel
    {
        public int SalaryId { get; set; }
        public double EmployeeTotalAmount { get; set; }
        public DateTime SalaryMonth { get; set; }
        public double AllowanceAmount { get; set; }
        [Required(ErrorMessage =" Please Select a EmployeeId ")]
        public int EmployeeId { get; set; }
        [Display(Name = "Allowance Type")]
        public string AllowanceType { get; set; }
        public Double BasicSalary { get; set; }
        public Double TotalAmount { get; set; }
        public double Deduction { get; set; }
        public int TotalAbsent { get; set; }
        public double NetAmount { get; set; }
        public double deducAmount { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int hid { get; set; }
        public string DeductionName { get; set; }
        public Nullable<double> DeductionAmount { get; set; }
    }
}