﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace payrollado.Models
{
    public class EmployeeModal
    {
        public int EmployeeId { get; set; }
        public int EmployeeIdfk { get; set; }
        [Display(Name = "First Name")]
        public string FirstName { get; set;}
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
        [EmailAddress(ErrorMessage ="Please Enter A Valid Email")]
        public string Email { get; set; }       
        public string Address { get; set; }
        [Display(Name = "Contact NO")]
        [RegularExpression("^[0-9]\\d*$", ErrorMessage = "Contact No Must Be a Number")]
        [StringLength(11, MinimumLength = 11, ErrorMessage = "Contact Number Must Contain 11 digits")]
        public string ContactNO { get; set; }
        [RegularExpression("([1-9][0-9]*)", ErrorMessage = "NIC must be a  Number")]
        [StringLength(13, MinimumLength = 13, ErrorMessage = "NIC Number Must Contain 13 digits")]
        public string NIC { get; set; }
        //[DataType(DataType.Date)]
        //[DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name = "Date of Birth")]
        public Nullable<DateTime> DateofBirth { get; set; }
        //[DataType(DataType.Date)]
        //[DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name = "Date of Joinning")]
        public Nullable<DateTime> DateofJoining { get; set; }
        [Display(Name = "Image File")]
        public string ImageFile { get; set; }
        [Display(Name ="Department")]
        public Nullable<int> DepartmentId { get; set; }        
        [Display(Name = "Designation")]
        public Nullable<int> Designation_Id { get; set; }
        [Display(Name = "Created BY")]
        public int CreatedBY { get; set; }
        [Display(Name = "Created At")]
        public DateTime CreatedAt { get; set; }
        public  bool IsActive { get; set; }
        [Display(Name = "Employee Education")]
        public int EmployeeEducationId { get; set; }
        [Display(Name = "Last Degree")]
        public string LastDegree { get; set; }
        public string Institute { get; set; }   
        public string Grade { get; set; }
        public int hid { get; set; }
        public string Division { get; set; }
        [RegularExpression("([1-9][0-9]*)", ErrorMessage = "Percentage Must Be a Number")]
        public Nullable <double> Percentage { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm}", ApplyFormatInEditMode = true)]
        [Display(Name = "Passing Year")]
        public Nullable<DateTime> PassingYear { get; set; }            
        public int ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }      
        public string Gender { get; set; }
        public string DepartmentName { get; set; }
        public string DesignationName { get; set; }
        [RegularExpression("^[0-9]\\d*$", ErrorMessage = "Contact No Must Be a Number")]
        public int EmployeeTypeId { get; set; }
        public string EmployeeTypeName { get; set; }

    }
}