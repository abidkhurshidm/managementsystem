﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace payrollado.Models
{
    public class SetupModel
    {
        [Display(Name = "Department Id")]
        public int DepartmentId { get; set; }
        [Display(Name = "Department Name")]
        public string Name { get; set; }
        [Display(Name = "Employee Id")]
        public Nullable<int> EmployeeId { get; set; }
        public int hidep { get; set; }
        [Display(Name = " Designation ")]
        public int Designation_Id { get; set; }
        [Display(Name = "Designation Name")]
        public string DesName { get; set;}
        [Display(Name = "Department Name")]
        public string DepartmentName { get; set; }
        [Display(Name = "Department Name")]
        public Nullable<int> fkDepartmentId { get; set; }
        public int hides { get; set; }
        [Display(Name = "Allowance Type")]
        public int AllowancTypeid { get; set; }
        [Display(Name = "Allowance Type")]
        public string allowanceType { get; set; }
        [Display(Name = "Employee Type")]
        public int EmployeeTypeId { get; set; }
        public int hidalw { get; set; }
        [Display(Name = "Leave Type")]
        public int LeaveTypeid { get; set; }
        public string LeaveType { get; set; }
        [Display(Name = "Deduction")]
        public int DeductionId { get; set; }
        [Display(Name = "Deduction Name")]
        public string DedeuctionName { get; set; }
        public int hidlev { get; set; }
        public int hidduc { get; set; }
        [Display(Name = "Employee Type")]
        public string EmployeeTypeName { get; set; }
        public int hidemp { get; set; }
        public ICollection<Holiday> events { get; set; }
        public int hdEventID { get; set;}
        public int eventid { get; set; }
        public string title { get; set; }
        public DateTime start { get; set; }
        public DateTime end { get; set; }
        public string color { get; set;}
        public bool allday { get; set; }
        public string description { get; set; }
    }
}