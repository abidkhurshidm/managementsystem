﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace payrollado.Models
{
    public class EmployeeSalaryM
    {
        public Nullable<int> EmployeeSalaryId { get; set; }
        [RegularExpression("([1-9][0-9]*)", ErrorMessage = "Enter only numeric number")]        
        [Display(Name = "Basic Salary")]
        public Nullable<double> BasicSelary { get; set; }
        [Display(Name = "Total Amount")]
        public double TotalAmount { get; set; }
        [Display(Name = "Employee")]
        public Nullable<int> EmployeeId { get; set; }
        public string EmployeeIdName { get; set; }
        [Display(Name = "Created By")]
        public int CreatedBy { get; set; }
        [Display(Name = "Created At")]
        public DateTime CreatedAt { get; set; }
        [Display(Name = "Allowance Type")]
        public int AllowanceTypeIdlabel { get; set; }
        [Display(Name = "Allowance Type")]
        [Required(ErrorMessage = "Please Select Allowance Type")]
        public int AllowanceTypeId { get; set; }
        public string AllowanceTypeName { get; set; }
        [Display(Name = "Deduction")]
        public int DeductionIdlabel { get; set; }
        [Display(Name = "Deduction")]
        [Required (ErrorMessage ="Please Select Deduction ")]
        public int DeductionId { get; set; }
        public string DeductionIdName { get; set; }
        public string DeductionName { get; set; }
        public double Amount { get; set; }
        [Display(Name = "Employee Type")]
        public Nullable<int> EmployeeTypeId { get; set; }

        public int allowancehid { get; set; }
        public int leavehid { get; set; }
        public int deductionhid { get; set; }

        public int allowancehid2 { get; set; }
        public int leavehid2 { get; set; }
        public int deductionhid2 { get; set; }





        public int hid { get; set; }

        public string EmployeeTypeIdName { get; set; }
        [Display(Name = "Leave Type")]
        public int LeaveTypeIdlabel { get; set; }
        [Display(Name = "Leave Type")]
        [Required(ErrorMessage = "Please Select Leave Type ")]
        public int LeaveTypeId { get; set; }
        [Display(Name = "Leave Name")]
        public string LeaveName { get; set; }
        [Display(Name = "Allowance Type")]
        public string AllowanceType1 { get; set; }
        public ICollection<sal_Result> sals { get; set; }
        [RegularExpression("([1-9][0-9]*)", ErrorMessage = "Enter only numeric number")]
        [Display(Name = "Alloawnce Amount")]

        public Nullable<double> AllowanceAmount { get; set; }

        public Nullable<double> AllowanceAmountlabel { get; set; }

        public Nullable<double> DeductionAmountlabel { get; set; }

        [RegularExpression("([1-9][0-9]*)", ErrorMessage = "Enter only numeric number")]
        [Display(Name = "Deduction Amount")]

        public Nullable<double> DeductionAmount { get; set; }

        [RegularExpression("([1-9][0-9]*)", ErrorMessage = "Enter only numeric number")]
        [Display(Name = "Leave Days")]
        public Nullable<double> LeaveDays { get; set; }

        public string LeaveTypeIdName { get; set; }

        public List<EmployeeAllowenceM> employeeAllowances { get; set; }

        public List<EmployeedeductionM> employeeDeductions { get; set; }

        public List<EmployeeleaveM> EmployeeLeaves { get; set; }

        public List<Sallist> sallists  { get; set; }

        public List<Deletealw> deletealws { get; set; }
        public List<Deletelev> deletelevs  { get; set; }
        public List<Deleteduc> deleteducs  { get; set; }

        public string leavetypelabel { get; set; }

        public string Deductiontypelabel { get; set; }

        public string AllowanceTypelabel { get; set; }

        public string detail { get; set; }

    }

    public class Deletealw
    {
        public int EmployeeAllowanceId { get; set; }
    }
    public class Deletelev
    {
        public int EmployeeLeaveTypeId { get; set; }

    }
    public class Deleteduc
    {
        public int EmployeeDedeuctionId { get; set; }
    }
    //public int EmployeeLeaveTypeId { get; set; }
    //public int EmployeeDedeuctionId { get; set; }

     public class Sallist
    {
        public Nullable<int> EmployeeSalaryId { get; set; }
        public int EmployeeId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public double TotalAmount { get; set; }
        public double BasicSelary { get; set; }
    }


    public class EmployeeAllowenceM
    {
        public int EmployeeAllowanceId { get; set; }
        public Nullable<double> AllowanceAmount { get; set; }
        public Nullable<int> EmployeeSalaryId { get; set; }
        public Nullable<int> AllowanceTypeId { get; set; }
        public string AllowenceTypeName { get; set; }
        public int EmployeeTypeid { get; set; }
        public int Employeeallowancehid { get; set; }
    }
    public class EmployeeleaveM
    {
        public int EmployeeLeaveTypeId { get; set; }
        public Nullable<double> LeaveDays { get; set; }
        public Nullable<int> LeaveTypeId { get; set; }
        public Nullable<int> EmployeeSalaryId { get; set; }
        public string leaveTypeName { get; set; }
        public int Employeeleavehid { get; set; }
    }
    public class EmployeedeductionM
    {
        public int EmployeeDedeuctionId { get; set; }
        public double DeductionAmount { get; set; }
        public Nullable<int> EmployeeSalaryId { get; set; }
        public Nullable<int> DeductionId { get; set; }
        public string deductionTypeName { get; set; }
        public int Employeedeductionhid { get; set; }

    }
}