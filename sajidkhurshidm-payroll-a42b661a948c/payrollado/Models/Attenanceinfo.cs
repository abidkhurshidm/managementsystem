﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace payrollado.Models
{
    public class Attenanceinfo
    {
        public string AttDate { get; set; }
        public ICollection<Employeeinfo> Employees { get; set; }
    }
}