﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace payrollado.Models
{
    public class Modal
    {
        public IEnumerable<Employee> employees { get; set; }
        public IEnumerable<EmployeeEducation> employeeEducations { get; set; }
        public IEnumerable<EmployeeSalary> employeeSalaries { get; set; }
    }
}