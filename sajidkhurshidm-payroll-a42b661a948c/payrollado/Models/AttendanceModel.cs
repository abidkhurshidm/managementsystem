﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace payrollado.Models
{
    public class AttendanceModel
    {
        public int AttendanceId { get; set; }
        public Nullable<int> MarkAttendance { get; set; }
        [Display(Name = "Employee")]
       // [Required(ErrorMessage = "Please Select A Employee")]
        public Nullable<int> EmployeeId { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
      //  [Required(ErrorMessage = " Please Select a ToDate ")]
        [Display(Name = "From date")]
        public Nullable<System.DateTime> AttendanceDatef { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage = " Please Select a ToDate ")]
        [Display(Name = "To Date")]
        public Nullable<System.DateTime> AttendanceDatet { get; set; }
        [Display(Name = "Leave Id")]
        public int LeaveId { get; set; }
        [Required(ErrorMessage = " Please give A Reason")]
        public string Reason { get; set; }       
        public HttpPostedFileBase AttachmentFile { get; set; }
        public string Attachment { get; set; }
       // [Required(ErrorMessage = " Please Select A LeaveType")]
        [Display(Name = "Leave Type")]
        public Nullable<int> LeaveTypeId { get; set; }
        public string AttachmentUrl { get; set; }
        [Display(Name = "From Date")]
        public string fdate { get; set; }
        [Display(Name = "To Date")]
        public string tdate { get; set; }
        [Display(Name = "Employee Type")]
        public int EmployeeTypeId { get; set; }
        public string EmployeeName { get; set; }
        [Display(Name = "leave Days")]
        public int leaveDays { get; set; }
        public int hid { get; set; }
        public int levhid { get; set; }
        [Display(Name = "Leave Deduct")]
        public Nullable<int> leaveDeduct { get; set; }
        public Nullable<int> rleaves { get; set; }
        [Display(Name = "Remianing Leaves")]
        public Nullable<int> Remianingleaves { get; set;}
        public ICollection<GetAttendance_Result> attendances { get; set; }
        public ICollection<leavesp_Result> leavesp_Results { get; set; }
        public IEnumerable<Leave> leaves { get; set; }
        public int attendancetype { get; set; }
        public string json { get; set; }

    }
}