﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace payrollado.Models
{
    public class Model2
    {
        public IEnumerable<EmployeeSalary> employeeSalaries { get; set; }
        public IEnumerable<AllowanceType> allowanceTypes { get; set; }
        public IEnumerable<LeaveType> LeaveTypes { get; set; }
        public IEnumerable<Deduction> deductions { get; set; }
    }
}