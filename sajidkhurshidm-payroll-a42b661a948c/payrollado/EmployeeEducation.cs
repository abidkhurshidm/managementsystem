//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace payrollado
{
    using System;
    using System.Collections.Generic;
    
    public partial class EmployeeEducation
    {
        public int EmployeeEducationId { get; set; }
        public string LastDegree { get; set; }
        public string Institute { get; set; }
        public string Grade { get; set; }
        public string Division { get; set; }
        public Nullable<double> Percentage { get; set; }
        public Nullable<System.DateTime> PassingYear { get; set; }
        public Nullable<int> Employeeid { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedAt { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
    
        public virtual Employee Employee { get; set; }
    }
}
